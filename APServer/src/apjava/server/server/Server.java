package apjava.server.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import apjava.lib.Session;
import apjava.server.view.ServerView;

public class Server extends Thread {
	private ServerSocket serverSocket;
	private int clientIdCnt;
	private ServerView serverView;
	private boolean running;
	private int port;
	//private ExecutorService executor = Executors.newFixedThreadPool(Configuration.threadPoolSize);
	
	/**
	 * 
	 * @param serverView
	 */
	public Server(ServerView serverView, int port) {
		setServerView(serverView);
		setRunning(false);
		setPort(port);
		Session.initializeSessionMap();
	}
	
	/**
	 * @return the clientIdCnt
	 */
	public int getClientIdCnt() {
		return clientIdCnt;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @param clientIdCnt the clientIdCnt to set
	 */
	public void setClientIdCnt(int clientIdCnt) {
		this.clientIdCnt = clientIdCnt;
	}

	/**
	 * @return the serverSocket
	 */
	public ServerSocket getServerSocket() {
		return serverSocket;
	}

	/**
	 * @param serverSocket the serverSocket to set
	 */
	public void setServerSocket(ServerSocket serverSocket) {
		this.serverSocket = serverSocket;
	}

	/**
	 * @return the serverView
	 */
	public ServerView getServerView() {
		return serverView;
	}

	/**
	 * @param serverView the serverView to set
	 */
	public void setServerView(ServerView serverView) {
		this.serverView = serverView;
	}

	/**
	 * @return the running
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * @param running the running to set
	 */
	public void setRunning(boolean running) {
		this.running = running;
	}
	
	/**
	 * 
	 * @param e
	 */
	public void showException(Exception e) {
		e.printStackTrace();
		serverView.sendText(e.getClass().getSimpleName()+" : "+ e.getMessage());			
	}


	public void serverStartMessage() {
		serverView.sendText("Server listening on: "+serverSocket.getLocalSocketAddress());
	}

	
	public void run() {
		setRunning(true);
		while(isRunning()) {
			try {
				if(serverSocket != null) {
					Socket socket = serverSocket.accept();
					clientIdCnt++;
					ServerRequestHander handler = new ServerRequestHander(this, socket, clientIdCnt);
					new Thread(handler).start();
					//executor.submit(handler);
				}
			} catch (IOException e) {
				showException(e);
			}
		}
	}

	public void startServer() {
		try {
			serverSocket = new ServerSocket(port);
			this.start();
			serverStartMessage();
		} catch (IOException e) {
			showException(e);
		}
	}
	
    public void stopServer()
    {
        setRunning(false);
        this.interrupt();
    }
	
}
