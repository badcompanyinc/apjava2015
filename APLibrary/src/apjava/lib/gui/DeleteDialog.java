package apjava.lib.gui;

import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public class DeleteDialog  extends JDialog{
	
	private JOptionPane optionPane;
	
	public DeleteDialog(JFrame frame, Component locationComp)
	{
		super(frame, "Delete Confirmation", true);
		
		setOptionPane(new JOptionPane(
			    "Are you sure you want to delete this ?",
			    JOptionPane.QUESTION_MESSAGE,
			    JOptionPane.YES_NO_OPTION) 
		);
		
		setContentPane(optionPane);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	
		addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent we) {
		        //setLabel("Thwarted user attempt to close window.");
		    }
		});
		optionPane.addPropertyChangeListener(
		    new PropertyChangeListener() {
		        public void propertyChange(PropertyChangeEvent e) {
		            String prop = e.getPropertyName();
	
		            if (isVisible() 
		             && (e.getSource() == optionPane)
		             && (prop.equals(JOptionPane.VALUE_PROPERTY))) {
		                //If you were going to check something
		                //before closing the window, you'd do
		                //it here.
		                setVisible(false);
		            }
		        }
		    });

		//setSize(200,200);
		pack();
		setLocationRelativeTo(locationComp);
		setVisible(true);
	
	}

	/**
	 * @return the optionPane
	 */
	public JOptionPane getOptionPane() {
		return optionPane;
	}

	/**
	 * @param optionPane the optionPane to set
	 */
	public void setOptionPane(JOptionPane optionPane) {
		this.optionPane = optionPane;
	}
	
	public Object getResponse() {
		return getOptionPane().getValue();
	}

}

