package apjava.server.model;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import apjava.lib.BaseModel;
import apjava.lib.Configuration;
import apjava.lib.Crypto;
import apjava.lib.types.UniqueString;

public class UserModel extends BaseModel{
	
	private String firstName;
	private String lastName;
	private UniqueString email;
	private UniqueString userName;
	private String password;
	
	public UserModel()
	{
		super(UserModel.class, Configuration.database_path+Configuration.databaseName,
				Configuration.databaseCommit);
		
	}
	
	public UserModel(String firstName, String lastName, String email,
			String userName, String password) {
		super(UserModel.class, Configuration.database_path+Configuration.databaseName,
				Configuration.databaseCommit);
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setEmail(email);
		this.setUserName(userName);
		this.setPassword(password);
	}

	public UserModel(String data) {
		super(UserModel.class, Configuration.database_path+Configuration.databaseName,
				Configuration.databaseCommit);
		super.setFieldValues(data);
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email.getString();
	}

	/**
	 * 
	 * @param email
	 */
	public void setEmail(UniqueString email)
	{
		this.email = email;
	}
	
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		if(this.email==null) {
			setEmail(new UniqueString(""));
		}
		this.email.setString(email);
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName.getString();
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		if(this.userName==null) {
			this.userName =  new UniqueString("");
		}
		this.userName.setString(userName);
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = getPassword(password);
	}
	
	/**
	 * @param password the password to set
	 */
	public String getPassword(String password) {
		String pass = null;
		try {
			if(!password.isEmpty() ){
				pass = Crypto.hashPassword(Configuration.randomSalt, password, Configuration.ITERATION_NUMBER);
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pass;
	}
	
	public boolean authenticate() {
		if(userName == null) return false;
		HashMap<String, String> map = view("userName", userName.toString());
		if(map==null || (map!=null && map.get("password") == null)) 
			return false;
		else
			return map.get("password").equals(password);
	}
	
	public String view(){
		String user = null;
		HashMap<String, String> map = view("userName", userName.toString());
		if(map != null ) {
			map.remove("password");
			user = map.toString();
		}
		return user;
	}

}
