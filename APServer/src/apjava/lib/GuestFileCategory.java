package apjava.lib;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;

import docx4.ParseDocument;

public class GuestFileCategory {
	
	private final String [] financeWordList = {
			"finance", "money", "calculate", "profit", "average", "monthly", "cost", "pay", "interest", "deposit", "withdraw",
			"equal", "minus", "substract", "formula", "formulae"
	};
	
	
	private final String [] HRWordList = {
			"human", "resource", "hired", "fired", "promoted", "demoted","restricted", "access"
	};
	
	private final String [] athleticsWordList = {
			"athletics", "athletic", "sports", "meter", "hurdle", "sprint", "distance", "muscles", "injury", "doping", "medal",
			"medalist", "spikes", "gold", "silver", "bronze", "qualify", "final", "finalist", "record", "break"
	};
	
	private final String [] healthWordList = {
			"diabetes", "virus", "infection", "bacteria", "pharmacy", "medication", "doctor", "nurse", "patient", "sick", "health",
			"healthy"
	};
	
	
	private double financeCount;
	private double HRCount;
	private double athleticsCount;
	private double healthCount;
	private String category = "Finance";
	
	private List<String> documentWords;
	
	public GuestFileCategory(File file){
		ParseDocument doc = new ParseDocument();
		try {
			documentWords = doc.parseToPlainTextChunks(file, 400);
		} catch (IOException | SAXException | TikaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public double compareWordToDocumentWords(String [] words){
		int count = 0;
		for(String str : words){
			if(documentWords.contains(str)){
				count++;
			}
		}
		if(count > 0)
			return (words.length/count);
		return 0;
	}
	
	public void findCounts(){
		financeCount = compareWordToDocumentWords(financeWordList);
		HRCount = compareWordToDocumentWords(HRWordList); 
		athleticsCount = compareWordToDocumentWords(athleticsWordList); 
		healthCount = compareWordToDocumentWords(healthWordList); 
	}
	
	public double getLarget(){
		double largest = financeCount;
		
		if(HRCount > largest){
			largest = HRCount;
			category = "Human Resource";
		}else if(athleticsCount > largest){
			largest = athleticsCount;
			category = "Athletics";
		}else if(healthCount > largest){
			largest = healthCount;
			category = "Health";
		}
		
		return largest;
	}
	
	public String getCategory(){
		findCounts();
		if(getLarget() < 0.3){
			return "Auxiliary";
		} else {
			return category;
		}
	}
}
