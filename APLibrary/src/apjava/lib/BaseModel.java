package apjava.lib;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;




import apjava.lib.exceptions.IncorrectFieldValueSizeException;
import apjava.lib.types.UniqueString;

public class BaseModel extends Database implements Model{
	
	private String tableName;
	private String id;
		
	public BaseModel(Class<?> cls, String databaseName, boolean databaseCommit) {
		super(databaseName, databaseCommit);
		initialize(cls);
	}
	
	/**
	 * 
	 * @param cls
	 */
	public void initialize(Class<?> cls) {
		setTableName(cls.getSimpleName());
		if(getDatabase().connect() ) {
			create(generateTableSql(cls));
		}
		setId(null);	
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @param data
	 */
	public void setFieldValues(String data) {
		String [] values = data.split("&");
		for(String value : values) {
			String [] keyValue = value.split("=");
			if(keyValue.length == 2) {
				setField(keyValue[0], keyValue[1]);
			}
		}
	}
	
	/**
	 * 
	 * @param map
	 */
	public void setFieldValues(HashMap<String, String> map) {
		ArrayList<String> fields = getModelFields(this.getClass());
		for(String field : fields){
			String value = map.get(field);
			if(value != null) {
				setField(field, value);
			}
		}
	}
	
	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void setField(String key, String value) {
		//identify the method with that takes the argument of type Message
		Method method;
		try {
			method = this.getClass().getMethod("set"+correctCase(key), String.class);
			if(value.trim().isEmpty()) value = null;
			method.invoke(this, value);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param str
	 * @return
	 */
	public String toUpperFirstLetter(String str) {
        char[] stringArray = str.trim().toCharArray();
        stringArray[0] = Character.toUpperCase(stringArray[0]);
        str = new String(stringArray);
        return str;
	}
	
	/**
	 * 
	 * @param dateStr
	 * @return
	 */
	public Date getDateFromString(String dateStr) {
		DateFormat format = null;
		Date date = null;
		try {
			format = new SimpleDateFormat("YYYY-MM-DD", Locale.ENGLISH);
			date = format.parse(dateStr);
		} catch (ParseException e) {
			//date may be in a different format
			format = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
			try {
				date = (Date)format.parse(dateStr);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return date;
	}
	
	/**
	 * 
	 * @param str
	 * @return
	 */
	public String correctCase(String str) {
		String parts[] = str.split("_"); 
		String correct = "";
		for(String part : parts) {
			correct += toUpperFirstLetter(part);
		}
		return correct;
	}
	
	public String getLastInsert() {
		if(!getDatabase().connect()){
			return null;
		}
		
		try {
			return Integer.toString(getDatabase().getLastInsert(getTableName()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			getDatabase().close();
		}
		
		return null;
	}

	@Override
	public String save() {
		if(!getDatabase().connect()){
			return null;
		}
		
		String sql = "INSERT INTO  "+getTableName()+" ("+concatFields()+") "+
				  "VALUES("+concatFieldsValues()+" );";
		String error = null;
		try {
			getDatabase().insert(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			error = e.getMessage();
		} finally {
			getDatabase().close();
		}
		
		return error;
	}

	@Override
	public boolean update() {
		if(!getDatabase().connect()){
			return false;
		}
		
		boolean saved = false;
		
		try {
			String sql = "UPDATE "+getTableName()+" SET "+
					splitCombineFieldsValues()+
					" WHERE Id = "+getId()+
					";";
			getDatabase().update(sql);
			saved = true;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IncorrectFieldValueSizeException e) {
			e.printStackTrace();
		} finally {
			getDatabase().close();
		}
		return saved;
	}

	@Override
	public HashMap<String, String> view(String byColumn, String id) {
		
		HashMap<String, String> hashMap = null;
		
		if(!getDatabase().connect()){
			return hashMap;
		}
		
		try {
			
			String sql = "SELECT * FROM "+getTableName()+
					" WHERE "+getTableName()+"."+byColumn+" = "+coats(id)+";";

			getDatabase().select(sql);
			
			ResultSet result = getDatabase().getResult();
			result.next(); //position to the first in row
			hashMap = view(result);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			getDatabase().close();
		}
		
		return hashMap;
	}
	
	
	@Override
	public boolean delete() {
		if(!getDatabase().connect()){
			return false;
		}
		String sql = "DELETE FROM "+getTableName()+" WHERE Id = "+getId()+";";
		try {
			getDatabase().delete(sql);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			getDatabase().close();
		}
		return false;
	}
	
	public HashMap<String, String> view(ResultSet result) throws SQLException {
		ArrayList<String> strFields = getModelFields(this.getClass());
		HashMap<String, String> hashMap = new HashMap<String, String>(0);
		
		if( !result.isClosed() ){
			hashMap.put("Id", result.getString("Id"));
			for(String str : strFields){
				hashMap.put(str, result.getString(str));
			}
			return hashMap;
		}
		return null;
	}
	
	public ArrayList<String> viewAll(ResultSet results) throws SQLException {
		ArrayList<String> list = new ArrayList<String>(0);
		
		while(results.next()){
			list.add(view(results).toString());
		}
		return list;
	}

	@Override
	public Object viewAll() {
		Object objs = null;
		
		if(!getDatabase().connect()){
			return null;
		}
		
		try {
			
			String sql = "SELECT * FROM "+getTableName()+";";
			getDatabase().select(sql);
			objs = viewAll(getDatabase().getResult()).toString();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			getDatabase().close();
		}
		
		return objs;
	}

	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @param tableName the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public String splitCombineFieldsValues() throws IncorrectFieldValueSizeException
	{
		String str = "";
		String cvalues[] = concatFieldsValues().split(",");
		String cfields[] = concatFields().split(",");
		if(cvalues.length < cfields.length) {
			throw new IncorrectFieldValueSizeException();
		}
		
		for(int i = 0; i < cvalues.length; i++)
		{
			str += cfields[i]+"="+cvalues[i]+",";
		}
		return str.substring(0, str.length()-1);
	}
	
	public String concatFields() {
		String str = "";
		ArrayList<String> strFields = getModelFields(this.getClass());
		for(String field: strFields)
		{
			str+=field+",";
		}
		//remove , end of string
		return str.substring(0, str.length()-1);
	}

	public String concatFieldsValues() {
		String str = "";
		ArrayList<Object> strFieldsValues;
		try {
			strFieldsValues = getModelFieldsValues();
			for(Object fieldValue: strFieldsValues) {
				if(fieldValue != null) {
					if(fieldValue instanceof Date) {
						str+=coats(((Date)fieldValue).toString())+",";
					}
					else if(BaseModel.class.isAssignableFrom(fieldValue.getClass()) ) {
						str+=coats( String.valueOf( ((BaseModel)fieldValue).getId() ) )+",";
					}
					else {
						str+=coats( String.valueOf(fieldValue))+",";
					}
				} else {
					str+="NULL,"; //add comma for null fields
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		//remove , end of string
		return str.substring(0, str.length()-1);
	}
	
	public ArrayList<Object> getModelFieldsValues() throws IllegalArgumentException, IllegalAccessException {
		Field [] fields = this.getClass().getDeclaredFields();
		ArrayList<Object> objFieldsValues = new ArrayList<Object>(0);
		for(Field field : fields)
		{
			if(field.getModifiers() == Modifier.PRIVATE) {
				//change accessiblity to access private field type
				field.setAccessible(true);
				objFieldsValues.add(field.get(this));
			}
		}
		return objFieldsValues;
	}
	
	public static ArrayList<String> getModelFields(Class<?> model) {
		Field [] fields = model.getDeclaredFields();
		ArrayList<String> strFields = new ArrayList<String>(0);
		for(Field field : fields) {
			if(field.getModifiers() == Modifier.PRIVATE) {
				strFields.add(field.getName());
			}
		}
		return strFields;
	}
	
	/**
	 * @param model
	 * @return
	 */
	public static String generateTableSql(Class<?> model) {
		Field [] fields = model.getDeclaredFields();
		String sql = "CREATE TABLE IF NOT EXISTS "+ model.getSimpleName()
				+ "(Id INTEGER PRIMARY KEY   AUTOINCREMENT,";
		
		String sqlRefs = "";
		
		for(Field field : fields) {
			if(field.getModifiers() == Modifier.PRIVATE)
			{
				//field is of type Date
				if(field.getType().isAssignableFrom(Date.class) ) {
					sql += field.getName()+" DATE  NOT NULL,";
				}
				//field is of type Integer
				else if(field.getType().isAssignableFrom(Integer.class) ) {
					sql += field.getName()+" INT  NOT NULL,";	
				}
				//field is of type String
				else if(field.getType().isAssignableFrom(String.class) ) {
					sql += field.getName()+" CHAR(50)  NOT NULL,";	
				}
				//field is of type UniqueString
				else if(field.getType().isAssignableFrom(UniqueString.class) ) {
					sql += field.getName()+" CHAR(50)  NOT NULL UNIQUE,";	
				}
				//field is of type BasModel
				else if(field.getType().getSuperclass().isAssignableFrom(BaseModel.class) )
				{
					sql += field.getName()+" INT,"; //dont set to null ensure refernce via coding
					sqlRefs += "FOREIGN KEY ("+field.getName()+") REFERENCES "+ field.getType().getSimpleName() +"(id),";
				}
			}
		}
		
		sql += sqlRefs;
		//remove last ,
		sql = sql.substring(0, sql.length()-1);
		sql += ");";
		
		return sql;
	}

}
