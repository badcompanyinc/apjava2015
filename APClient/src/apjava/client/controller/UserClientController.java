package apjava.client.controller;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import apjava.client.view.UserView;
import apjava.lib.Client;
import apjava.lib.Configuration;
import apjava.lib.Message;
import apjava.lib.Middleware;

public class UserClientController extends ClientController{
	
	private UserView userView;
	
	public UserClientController() {
		setUserView(new UserView("UserView", this));
		setMainView(userView);
	}
	
	public String getUserInfo() {
		Object [] objs = getReply("User", "view", "", Message.textContentType, 30);
		return Middleware.getStringMessageBodyFromObject(objs);
	}
	
	public void loginAction() {
		Runnable runable =  new Runnable(){
			public void run(){
				String password = new String(userView.getLoginForm().getPasswordTF().getPassword());
				String username = userView.getLoginForm().getUsernameTF().getText();
				Object [] objs = getReply("User", "login", "user_name="+username+"&password="+password, Message.textContentType, 30);
				if(objs != null) {
					userView.getLoginForm().getMessageTF().setText(Middleware.getStringMessageBodyFromObject(objs));
					Message msg = Middleware.getMessageFromObject(objs);
					if(msg!= null && msg.getStatus().equals(Message.ok))
					{
						userView.getLoginForm().clear();
						AppHandler.redirect( FileClientController.class );
					}
				}
				else{
					userView.getLoginForm().getMessageTF().setText("Connection Error");
				}
				userView.enableLoginButton();
			}
		};
		(new Thread(runable)).start();
	}
	
	public void logoutAction()
	{
		Object [] objs = getReply("User", "logout","",Message.textContentType, 30);
		Message msg = Middleware.getMessageFromObject(objs);
		if(msg != null && msg.getStatus().equals(Message.ok))
		{
			Client.getClient(Configuration.server_hostname, Configuration.port).close();
		}
	}
	
	public void registerAction() {
		String fName= userView.getRegistrationForm().getFirstNameTxt().getText().trim();
		String lName= userView.getRegistrationForm().getLastNameTxt().getText().trim();
		String email= userView.getRegistrationForm().getEmailTxt().getText().trim();
		String password=new String(userView.getRegistrationForm().getPasswordTxt().getPassword()).trim();
		String userName=userView.getRegistrationForm().getUserNameTxt().getText().trim();
		
		String strmsg = "first_name="+fName+"&last_name="+lName+"&email="+email+"&user_name="+userName+"&password="+password;
		Object [] objs = getReply("User", "create", strmsg, Message.textContentType, 30);
		Message msg = Middleware.getMessageFromObject(objs);
		if(msg!= null)
		{
			userView.getRegistrationForm().getMessageTF().setText(Middleware.getStringMessageBodyFromObject(objs));
			if(msg.getStatus().equals(Message.ok)) 
				{
					JOptionPane.showMessageDialog(this.getMainView(), "Registration Successful", "Success",JOptionPane.INFORMATION_MESSAGE);
					userView.getRegistrationForm().clear();
					
					//show the login form
					userView.remove( userView.getRegistrationForm().getRootPanel());
					userView.add( userView.getLoginForm().getRootPanel() );
					userView.getExtraView().getLoginButton().setVisible(false);
					userView.getExtraView().getRegButton().setVisible(true);
				}
		}
		userView.enableRegistrationButton();
	}

	/**
	 * @return the userView
	 */
	public UserView getUserView() {
		return userView;
	}

	/**
	 * @param userView the userView to set
	 */
	public void setUserView(UserView userView) {
		this.userView = userView;
	}

}
