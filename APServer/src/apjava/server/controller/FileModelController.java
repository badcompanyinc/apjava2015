package apjava.server.controller;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import apjava.lib.Configuration;
import apjava.lib.FileInfo;
import apjava.lib.GuestFileCategory;
import apjava.lib.Helper;
import apjava.lib.Message;
import apjava.lib.Session;
import apjava.server.model.FileModel;

public class FileModelController extends ModelController{
	
	public Message upload(Session session) {
		FileModel model = new FileModel("owner="+Helper.convertToHashMap((String)session.getValue("authUser")).get("Id")+
										"&"+session.getValue("request.file") 
										);
		//change the location of file to upload path
		File file = new File(model.getLocation());
		String new_path = Configuration.upload_path+file.getName();
		file.renameTo(new File(new_path));
		model.setLocation(new_path);
		
		//Guest the file category if no id was passed
		if(model.getCategory().getId().isEmpty()){
			String name = new GuestFileCategory(new File(new_path)).getCategory();
			int cnt = 0;
			for(String str : Configuration.default_categories){
				if(str.equals(name)){
					model.getCategory().setId(""+cnt);
					break;
				}
				cnt++;
			}
		}
		
		String error = model.save();
		if(error == null) {
			model = new FileModel("Id="+model.getLastInsert());
			return session.flashOk(model.view().toString());
		} else{
			//delete the file if failed to upload correctly
			new File(model.getLocation()).delete();
			return session.flashError("Unable to create new file."+error);
		}
	}
	
	
	public Message update(Session session) {
				
		FileModel model = new FileModel(session.getValue("request.file") + "");
		//initialize file with previous data
		File file = new File(model.getLocation());
		//get previous file details
		model.view();
		String path = model.getLocation();
		//remove old file path
		new File(path).delete();
		//rename the new file to the old file location
		file.renameTo(new File(path));
		//check if lastUpdate is current user
		if(!model.getLastUpdate().getId().equals(Helper.convertToHashMap((String)session.getValue("authUser")).get("Id")))
		{
			return session.flashError("Access denied. Try again later");
		}
		
		//set status of file to open for editing
		model.setStatus(0);
		model.setUpdated(new Date(System.currentTimeMillis()));
		
		if(model.update()) {
			return session.flashOk(model.view().toString());
		} else{
			return session.flashError("Unable to save file.");
		}
	}
	
	public Message viewAll(Session session) {
		FileModel model = new FileModel("category="+(String)session.getValue("request.body"));
		Object objs = model.viewAll();
		if(objs != null) {
				return session.flashOk( objs );
		} else {
			return session.flashNotFound("no values found");
		}
	}
	
	public Message view(Session session) {
		FileModel model = new FileModel("Id="+session.getValue("request.body"));
		Object objs = model.view();
		if(objs != null) {
			model.setLastView(Helper.convertToHashMap((String)session.getValue("authUser")).get("Id"));
			model.update();
			return session.flashOk( objs.toString() );
		} else {
			return session.flashNotFound("no values found");
		}
	}
	
	@SuppressWarnings("unchecked")
	public Message edit(Session session) {
		FileModel model = new FileModel("Id="+session.getValue("request.body"));
		HashMap<String, String> map = (HashMap<String, String>) model.view();
		if(map != null) {
			long timeNow = System.currentTimeMillis();
			long modelDate = model.getUpdated().getTime();
			long minutesPassed = TimeUnit.MILLISECONDS.toMinutes(timeNow-modelDate);
			if(model.getLastUpdate().equals(Helper.convertToHashMap((String)session.getValue("authUser")).get("Id"))
					|| model.getStatus() == 0 ||  minutesPassed > Configuration.timeAllowedToEdit) {
				if(new File(map.get("location")).exists()) {
					//update the status of the file
					model.setStatus(1);
					model.setLastUpdate(Helper.convertToHashMap((String)session.getValue("authUser")).get("Id"));
					model.update();
					FileInfo fileInfo = new FileInfo(new File(map.get("location")));
					return session.flashFileOk( fileInfo );
				} else {
					return session.flashNotFound("File Not found");
				}
			} else {
				return session.flashError("File Currently Opened");
			}
		} else {
			return session.flashNotFound("File Not found");
		}
	}
	
	@SuppressWarnings("unchecked")
	public Message download(Session session) {
		FileModel model = new FileModel("Id="+session.getValue("request.body"));
		HashMap<String, String> map = (HashMap<String, String>) model.view();
		if(map != null) {
			if(new File(map.get("location")).exists()){
				FileInfo fileInfo = new FileInfo(new File(map.get("location")));
				return session.flashFileOk( fileInfo );
			} else {
				return session.flashNotFound("File not found");
			}
		} else {
			return session.flashNotFound("File not found");
		}
	}
	
	@SuppressWarnings("unchecked")
	public Message delete(Session session) {
		FileModel model = new FileModel("Id="+session.getValue("request.body"));
		HashMap<String, String> map = (HashMap<String, String>) model.view();
		if(map != null){
			if(model.getStatus() == 0){
				File file = new File(map.get("location"));
				if(model.delete()){
					if(file.delete()) {
						return session.flashOk( "File removed" );
					} else {
						return session.flashError("Could not locate file to remove");
					}
				} else {
					return session.flashError("Error removing file from database");
				}
			} else {
				return session.flashError("File Currently Opened");
			}
		} else {
			return session.flashNotFound("No values found");
		}
	}

}
