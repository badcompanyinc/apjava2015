package apjava.client.view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import apjava.lib.gui.Form;

public class UserViewForm extends Form{
	private JLabel firstNameLbl;
	private JLabel lastNameLbl;
	private JLabel emailLbl;
	private JLabel userNameLbl;
	
	private JTextField firstNameTxt;
	private JTextField lastNameTxt;
	private JTextField emailTxt;
	private JTextField userNameTxt;
	
	
	public UserViewForm ()
	{
		//super();
		super(new FlowLayout(),"User Reg Info", "Close", "Clear");
	}



	/**
	 * @return the firstNameLbl
	 */
	public JLabel getFirstNameLbl() {
		return firstNameLbl;
	}



	/**
	 * @param firstNameLbl the firstNameLbl to set
	 */
	public void setFirstNameLbl(JLabel firstNameLbl) {
		this.firstNameLbl = firstNameLbl;
	}



	/**
	 * @return the lastNameLbl
	 */
	public JLabel getLastNameLbl() {
		return lastNameLbl;
	}



	/**
	 * @param lastNameLbl the lastNameLbl to set
	 */
	public void setLastNameLbl(JLabel lastNameLbl) {
		this.lastNameLbl = lastNameLbl;
	}



	/**
	 * @return the emailLbl
	 */
	public JLabel getEmailLbl() {
		return emailLbl;
	}



	/**
	 * @param emailLbl the emailLbl to set
	 */
	public void setEmailLbl(JLabel emailLbl) {
		this.emailLbl = emailLbl;
	}



	/**
	 * @return the userNameLbl
	 */
	public JLabel getUserNameLbl() {
		return userNameLbl;
	}



	/**
	 * @param userNameLbl the userNameLbl to set
	 */
	public void setUserNameLbl(JLabel userNameLbl) {
		this.userNameLbl = userNameLbl;
	}


	/**
	 * @return the firstNameTxt
	 */
	public JTextField getFirstNameTxt() {
		return firstNameTxt;
	}



	/**
	 * @param firstNameTxt the firstNameTxt to set
	 */
	public void setFirstNameTxt(JTextField firstNameTxt) {
		this.firstNameTxt = firstNameTxt;
	}



	/**
	 * @return the lastNameTxt
	 */
	public JTextField getLastNameTxt() {
		return lastNameTxt;
	}



	/**
	 * @param lastNameTxt the lastNameTxt to set
	 */
	public void setLastNameTxt(JTextField lastNameTxt) {
		this.lastNameTxt = lastNameTxt;
	}



	/**
	 * @return the emailTxt
	 */
	public JTextField getEmailTxt() {
		return emailTxt;
	}



	/**
	 * @param emailTxt the emailTxt to set
	 */
	public void setEmailTxt(JTextField emailTxt) {
		this.emailTxt = emailTxt;
	}



	/**
	 * @return the userNameTxt
	 */
	public JTextField getUserNameTxt() {
		return userNameTxt;
	}



	/**
	 * @param userNameTxt the userNameTxt to set
	 */
	public void setUserNameTxt(JTextField userNameTxt) {
		this.userNameTxt = userNameTxt;
	}

	@Override
	public void initializeComponents() {
		firstNameLbl=new JLabel("First Name");
		firstNameTxt = new JTextField(20);
		
		
		lastNameLbl =new JLabel("Last Name");
		lastNameTxt= new JTextField(20);
		
		
		emailLbl = new JLabel("Email");
		emailTxt = new JTextField(20);
		
		
		userNameLbl = new JLabel("User Name");
		userNameTxt= new JTextField(20);
		
		//
		firstNameTxt.setEditable(false);
		lastNameTxt.setEditable(false);
		emailTxt.setEditable(false);
		userNameTxt.setEditable(false);
		
		super.getClearBtn().setVisible(false);
		super.getSubmitBtn().setVisible(false);
	}


	@Override
	public void addComponentsToPanels() {

		add(firstNameLbl, firstNameTxt);
		add(lastNameLbl, lastNameTxt);
		add(emailLbl,emailTxt);
		add(userNameLbl,userNameTxt);
				
		getRootPanel().setBorder(emptyBorder(50, 100, 50, 100));
		
	}


	@Override
	public void registerListeners() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void clear() {
		this.getFirstNameTxt().setText("");
		this.getLastNameTxt().setText("");
		this.getUserNameTxt().setText("");
		this.getEmailTxt().setText("");
		this.getMessageTF().setText("");
	}
	
	
	public void setFormData(String firstName, String lastName, String userName, String emailTxt){
		this.getFirstNameTxt().setText(firstName);
		this.getLastNameTxt().setText(lastName);
		this.getUserNameTxt().setText(userName);
		this.getEmailTxt().setText(emailTxt);	
	}
	
}
