package docx4;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.BodyContentHandler;
import org.apache.tika.sax.ContentHandlerDecorator;
import org.apache.tika.sax.ToXMLContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public class ParseDocument {

	public String parseToString(File file) throws IOException, SAXException,
			TikaException {
		InputStream stream = new FileInputStream(file);
		AutoDetectParser parser = new AutoDetectParser();
		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();
		try {
			parser.parse(stream, handler, metadata);
			return handler.toString();
		} finally {
			stream.close();
		}
	}

	public List<String> parseToPlainTextChunks(File file, final int ChunkSize)
			throws IOException, SAXException, TikaException {
		final List<String> chunks = new ArrayList<String>();
		chunks.add("");
		ContentHandlerDecorator handler = new ContentHandlerDecorator() {
			@Override
			public void characters(char[] ch, int start, int length) {
				String lastChunk = chunks.get(chunks.size() - 1);
				String thisStr = new String(ch, start, length);

				if (lastChunk.length() + length > ChunkSize) {
					chunks.add(thisStr);
				} else {
					chunks.set(chunks.size() - 1, lastChunk + thisStr);
				}
			}
		};

		InputStream stream = new FileInputStream(file);
		AutoDetectParser parser = new AutoDetectParser();
		Metadata metadata = new Metadata();
		try {
			parser.parse(stream, handler, metadata);
			return chunks;
		} finally {
			stream.close();
		}
	}
	
	
	public String parseToHTML(File file) throws IOException, SAXException, TikaException {
	    ContentHandler handler = new ToXMLContentHandler();
	    InputStream stream = new FileInputStream(file);
	    AutoDetectParser parser = new AutoDetectParser();
	    Metadata metadata = new Metadata();
	    try {
	        parser.parse(stream, handler, metadata);
	        return handler.toString();
	    } finally {
	        stream.close();
	    }
	}

}
