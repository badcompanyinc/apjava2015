package apjava.lib.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;

import apjava.lib.Controller;

@SuppressWarnings("serial")
public abstract class View  extends JFrame implements ViewInterface{

	private Controller controller;
	private boolean setVisible;

	public View(String title, Controller controller)
	{
		super(title);
		setSetVisible(true);
		setController(controller);
		initializeComponents();
		addComponentsToPanels();
		addPanelsToFrame();
		registerListeners();
		setWindowProperties();
		addWindowListener();
	}
	
	private void addWindowListener() {
		this.addComponentListener(new ComponentAdapter(){
	        public void componentResized(ComponentEvent e){
	        	JFrame frame = (JFrame) (e.getSource());
	            Dimension d = frame.getSize();
	            Dimension minD = frame.getMinimumSize();
	            if(d.width < minD.width)
	                d.width = minD.width;
	            if(d.height < minD.height)
	                d.height = minD.height;
	            frame.setSize(d);
	        }
	    });
	}

	/**
	 * @return the setVisible
	 */
	public boolean isSetVisible() {
		return setVisible;
	}

	/**
	 * @param setVisible the setVisible to set
	 */
	public void setSetVisible(boolean setVisible) {
		this.setVisible = setVisible;
	}

	public abstract void addPanelsToFrame();
	
	public void registerListeners(Form form) {
		form.getClearBtn().addActionListener(this);
		form.getSubmitBtn().addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e, Form form) {
		if(e.getSource().equals(form.getClearBtn()))
		{
			form.clear();
		}
	}
	
	public void setWindowProperties()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(605, 500);
		setMinimumSize(new Dimension(605,500));
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(isSetVisible());
	}

	/**
	 * @return the controller
	 */
	public Controller getController() {
		return controller;
	}

	/**
	 * @param controller2 the controller to set
	 */
	public void setController(Controller controller2) {
		this.controller = controller2;
	}

	public abstract void actionPerformed(DefaultContextMenu menu, MouseEvent e);
	
	
	
}
