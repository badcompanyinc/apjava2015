package apjava.server.controller;

import apjava.lib.Message;
import apjava.lib.Session;
import apjava.server.model.CategoryModel;

public class CategoryModelController extends ModelController {

	public Message viewAll(Session session) {
		CategoryModel model = new CategoryModel();
		Object objs = model.viewAll();
		if(objs != null) {
				return session.flashOk( objs );
		} else {
			return session.flashNotFound("no values found");
		}
	}
	
}
