package apjava.lib;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Random;

public abstract class Middleware {
	
	public static void writeBytes(byte [] str, BufferedOutputStream out) throws IOException{
		if(out == null) return;
		out.write(str);
		out.flush();
	}
	
	public static void writeObject(Object obj, Socket socket) throws IOException{
		if(socket != null){
			ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
			outputStream.writeObject(obj);
			outputStream.flush();
		}
	}
	
	public static Message getMessageFromObject(Object [] objs) {
		return (Message)objs[0];
	}
	
	public static String getBufferOverflowFromObject(Object [] objs) {
		if(objs.length > 1) {
			return (String)objs[1];
		}
		return null;
	}
	
	public static Object getMessageBodyFromObject(Object [] objs) {
		if(objs != null && objs.length > 1) {
			return (String)objs[1];
		}
		return null;
	}
	
	public static String getStringMessageBodyFromObject(Object [] objs) {
		return (String) getMessageBodyFromObject(objs);
	}
	
	public static Object readObj(Socket socket) throws ClassNotFoundException, IOException
	{
		Object obj = null;
		ObjectInputStream objIputStream = new ObjectInputStream(socket.getInputStream());
		obj = objIputStream.readObject();
		return obj;
	}
	
	public static void sendMessage(Message head, Object objBody, Socket socket, BufferedOutputStream out) throws IOException {
		if(head.getContentType().equals(Message.fileContentType)) {

			FileInfo fileInfo = (FileInfo) objBody;
			
			head.setLength(fileInfo.getLength()); //set length of message
			head.setFileInfo(fileInfo); //set file info for fileContenttype message
			writeObject(head, socket);
			
	        FileInputStream fis = new FileInputStream(fileInfo.getLocation());
	        BufferedInputStream reader = new BufferedInputStream(fis);

			while(reader.available() > 0) {
				byte [] buffer = new byte[1024];
				int l = readInputStreamWithTimeout(reader, buffer, 300);
				if(l < 0) break;
				out.write(buffer, 0, l);
				out.flush();
			}
			reader.close();
			fis.close();
		}
		else if(head.getContentType().equals(Message.textContentType)) {
			byte [] bytes = Serializer.serialize(objBody);
			head.setLength(bytes.length);
			writeObject(head, socket);
			writeBytes(bytes, out);
			
		}
	}
	
	public static Object readObjBody(Message message, Object prevObj, Socket socket, BufferedInputStream reader) throws IOException, ClassNotFoundException {
		//check if object as file and save to tmp location
		//expecting a file here try to read and save to tmp location
		Object objBody = null;
		
		if( message.getContentType().equals(Message.fileContentType)) {
			
			FileOutputStream fileOutputStream = null;
			long length = 0;
			if(prevObj instanceof String) {
				String textBody = ((String)prevObj);
				length = textBody.length();
				
				String tmpFileName = Configuration.tmp_path+"temp"+new Random().nextLong();
				fileOutputStream = new FileOutputStream(tmpFileName, true );
				
				if(length > 0) {
					fileOutputStream.write( textBody.getBytes());
				}
				objBody = new File(tmpFileName);
			}
			else if (prevObj instanceof File) {
				length = ((File)prevObj).length();
				fileOutputStream = new FileOutputStream((File)prevObj, true );
				objBody = (File)prevObj;
			}
			
			while(reader.available() > 0) {
				byte [] buffer = new byte[1024];
				int l = readInputStreamWithTimeout(reader, buffer, 30);
				if(l < 0) break;
				fileOutputStream.write(buffer, 0, l);
				fileOutputStream.flush();
			}
			
			if(length == message.getLength()) {
				message.setReady(true);
				//change the location of the file to the new location on the server
				if(prevObj instanceof File) {
					message.getFileInfo().setLocation(((File)prevObj).getAbsolutePath());
				}
			}

			fileOutputStream.close();
			
		} else if( message.getContentType().equals(Message.textContentType)) {
			String textBody = ((String)prevObj);

			//BufferedReader readReady = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			while(reader.available() > 0) {
				byte [] buffer = new byte[1024];
				int l = readInputStreamWithTimeout(reader, buffer, 30);
				if(l < 0) break;
				textBody+=new String(buffer).substring(0, l);
			}
			
			if(textBody.length() == message.getLength()) {
				message.setReady(true);
				objBody = Serializer.deserialize(textBody.getBytes());
			}
			else {
				objBody = textBody;
			}
		}
		
		return objBody;
	}
	
	//http://stackoverflow.com/questions/804951/is-it-possible-to-read-from-a-inputstream-with-a-timeout
	public static int readInputStreamWithTimeout(InputStream is, byte[] b, int timeoutMillis) throws IOException  {
	     int bufferOffset = 0;
	     long maxTimeMillis = System.currentTimeMillis() + timeoutMillis;
	     while (System.currentTimeMillis() < maxTimeMillis && bufferOffset < b.length) {
	         int readLength = java.lang.Math.min(is.available(),b.length-bufferOffset);
	         int readResult = is.read(b, bufferOffset, readLength);
	         if (readResult == -1) break;
	         bufferOffset += readResult;
	     }
	     return bufferOffset;
	}
}
