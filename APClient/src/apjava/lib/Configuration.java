package apjava.lib;

import javax.swing.filechooser.FileNameExtensionFilter;

public class Configuration {
	
	/*
	 * The configuration
	 */
	public static final int port = 1354;
	public static String server_hostname = "127.0.0.1";//"192.168.43.14";//
	public static String dateFormate = "YYYY-MM-DD";
	public static String tmp_path = "./tmp/";
	public static String icons_path = "./icons/";
	public static String icons_cache_path = "./icons/cache/";
	public static String database_path = "./database/";
	public static String [] acceptedExtension = {"pdf", "doc", "docx", "csv", "xls", "xlsx", "txt"};
	//backup file paths
	public static String tmp_edit_path = tmp_path + "edit/";
	public static long timeAllowedToEdit = 5;
	
}
