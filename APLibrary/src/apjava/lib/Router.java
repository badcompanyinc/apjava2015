package apjava.lib;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Used to route traffic to correct controller and method
 * @author badcompany
 *
 */
public class Router {
	private Class<?> cls = null;
	
	public Router()
	{
		
	}
	
	public Message route(Message message, Session session) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, InstantiationException
	{
		cls = Class.forName("apjava.server.controller."+message.getController()+"ModelController");
		//identify the method with that takes the argument of type Message
		Method method = cls.getMethod(message.getAction(), Session.class);
		return (Message)method.invoke(cls.newInstance(), session);
	}
}
