package apjava.client.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import apjava.client.controller.UserClientController;
import apjava.lib.Controller;
import apjava.lib.gui.DefaultContextMenu;
import apjava.lib.gui.Form;
import apjava.lib.gui.View;

@SuppressWarnings("serial")
public class UserView extends View{
	
	private LoginForm loginForm;
	private RegistrationForm registrationForm;
	private UserExtraView extraView;

	public UserView(String title, Controller controller) {
		super(title, controller);
		setLayout(new BorderLayout());
	}

	/**
	 * @return the extraView
	 */
	public UserExtraView getExtraView() {
		return extraView;
	}

	/**
	 * @param extraView the extraView to set
	 */
	public void setExtraView(UserExtraView extraView) {
		this.extraView = extraView;
	}

	/**
	 * @return the registrationForm
	 */
	public RegistrationForm getRegistrationForm() {
		return registrationForm;
	}

	/**
	 * @param registrationForm the registrationForm to set
	 */
	public void setRegistrationForm(RegistrationForm registrationForm) {
		this.registrationForm = registrationForm;
	}

	/**
	 * @return the loginForm
	 */
	public LoginForm getLoginForm() {
		return loginForm;
	}

	/**
	 * @param loginForm the loginForm to set
	 */
	public void setLoginForm(LoginForm loginForm) {
		this.loginForm = loginForm;
	}

	@Override
	public void initializeComponents() {
		setLoginForm(new LoginForm());
		setRegistrationForm(new RegistrationForm());
		setExtraView(new UserExtraView(getController()));
		getExtraView().getLoginButton().setVisible(false);
		getExtraView().getRegButton().setVisible(true);
	}

	@Override
	public void addComponentsToPanels() {

	}

	@Override
	public void registerListeners() {
		super.registerListeners(getLoginForm());
		super.registerListeners(getRegistrationForm());
	}
	
	
	public void enableLoginButton() {
		Form.enableButton(getLoginForm().getSubmitBtn());
	}

	public void enableRegistrationButton() {
		Form.enableButton(getRegistrationForm().getSubmitBtn());
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e, getLoginForm());
		super.actionPerformed(e, getRegistrationForm());
		if(e.getSource().equals(getLoginForm().getSubmitBtn())){	
			SwingUtilities.invokeLater( new Runnable(){
				public void run(){
					Form.disableButton(getLoginForm().getSubmitBtn());
					((UserClientController)getController()).loginAction();
				}
			});
		}
		else
			if(e.getSource().equals(getRegistrationForm().getSubmitBtn())) {
				SwingUtilities.invokeLater( new Runnable(){
					public void run(){
						Form.disableButton(getRegistrationForm().getSubmitBtn());
						((UserClientController)getController()).registerAction();
					}
				});
			}
	}

	@Override
	public void addPanelsToFrame() {
		add(getRegistrationForm().getRootPanel(), BorderLayout.CENTER);
		add(getLoginForm().getRootPanel(), BorderLayout.CENTER);
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(getExtraView().getRootPanel(), BorderLayout.EAST);
		add(panel, BorderLayout.NORTH);
	}

	@Override
	public void actionPerformed(DefaultContextMenu menu, MouseEvent e) {

	}



}
