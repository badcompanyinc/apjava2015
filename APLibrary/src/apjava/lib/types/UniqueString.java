package apjava.lib.types;

public final class UniqueString {
	private String string;
	
	public UniqueString(String string)
	{
		this.setString(string);
	}

	/**
	 * @return the string
	 */
	public String getString() {
		return string;
	}

	/**
	 * @param string the string to set
	 */
	public void setString(String string) {
		this.string = string;
	}
	
	/**
	 * 
	 */
	public String toString() {
		return getString();
	}
}
