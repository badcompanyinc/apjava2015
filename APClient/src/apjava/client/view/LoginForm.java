package apjava.client.view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import apjava.lib.gui.Form;

public class LoginForm extends Form {
	private JTextField usernameTF;
	private JLabel userNameLbl;
	private JPasswordField passwordTF;
	private JLabel passwordLbl;
	
	public LoginForm()
	{		
		super(new FlowLayout(),"Login Form","Login","Clear");
	}

	/**
	 * @return the usernameTF
	 */
	public JTextField getUsernameTF() {
		return usernameTF;
	}

	/**
	 * @param usernameTF the usernameTF to set
	 */
	public void setUsernameTF(JTextField usernameTF) {
		this.usernameTF = usernameTF;
	}

	/**
	 * @return the passwordTF
	 */
	public JPasswordField getPasswordTF() {
		return passwordTF;
	}

	/**
	 * @param passwordTF the passwordTF to set
	 */
	public void setPasswordTF(JPasswordField passwordTF) {
		this.passwordTF = passwordTF;
	}
	
	/**
	 * @return the userNameLbl
	 */
	public JLabel getUserNameLbl() {
		return userNameLbl;
	}

	/**
	 * @param userNameLbl the userNameLbl to set
	 */
	public void setUserNameLbl(JLabel userNameLbl) {
		this.userNameLbl = userNameLbl;
	}

	/**
	 * @return the passwordLbl
	 */
	public JLabel getPasswordLbl() {
		return passwordLbl;
	}

	/**
	 * @param passwordLbl the passwordLbl to set
	 */
	public void setPasswordLbl(JLabel passwordLbl) {
		this.passwordLbl = passwordLbl;
	}

	@Override
	public void initializeComponents() {
		
		setUsernameTF(new JTextField(20));
		setPasswordTF(new JPasswordField(20));
		setUserNameLbl(new JLabel("User Name"));
		setPasswordLbl(new JLabel("Password"));
	}

	@Override
	public void addComponentsToPanels() {
		add(userNameLbl,usernameTF);
		add(passwordLbl,passwordTF);
		getRootPanel().setBorder(emptyBorder(50, 100, 190, 100));
	}
	
	
	@Override
	public void registerListeners() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		this.getUsernameTF().setText("");
		this.getPasswordTF().setText("");
		this.getMessageTF().setText("");
	}

	
}
