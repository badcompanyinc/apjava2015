package apjava.server.model;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

import apjava.lib.BaseModel;
import apjava.lib.Configuration;

public class FileModel extends BaseModel{
	//references not enforcing not null
	//no need to pre initialize the category or the user models
	private CategoryModel category;
	private UserModel owner;
	private UserModel lastView;
	private UserModel lastUpdate;
	private String name;
	private Integer size;
	private String extension;
	private String location;
	private Integer status;
	private Date created;
	private Date updated;
	
	public FileModel()
	{
		super(FileModel.class, Configuration.database_path+Configuration.databaseName,
				Configuration.databaseCommit);
		setCategory(new CategoryModel());
		setOwner(new UserModel());
		setLastView(new UserModel());
		setLastUpdate(new UserModel());
		setName("");
		setSize(0);
		setExtension("");
		setLocation("");
		setStatus(0);
		setCreated(new Date());
		setUpdated(new Date());
	}
	
	
	public FileModel(CategoryModel category, UserModel owner,
			UserModel lastView, UserModel lastUpdate, String name, int size,
			String extension, String location, int status, Date created,
			Date updated) {
		super(FileModel.class, Configuration.database_path+Configuration.databaseName,
				Configuration.databaseCommit);
		setCategory(category);
		setOwner(owner);
		setLastView(lastView);
		setLastUpdate(lastUpdate);
		setName(name);
		setSize(size);
		setExtension(extension);
		setLocation(location);
		setStatus(status);
		setCreated(created);
		setUpdated(updated);
	}
	
	public FileModel(String data) {
		super(FileModel.class, Configuration.database_path+Configuration.databaseName,
				Configuration.databaseCommit);
		setCategory(new CategoryModel());
		setOwner(new UserModel());
		setLastView(new UserModel());
		setLastUpdate(new UserModel());
		setName("");
		setSize(0);
		setExtension("");
		setLocation("");
		setStatus(0);
		setCreated(new Date());
		setUpdated(new Date());
		super.setFieldValues(data);
	}


	/**
	 * @return the category
	 */
	public CategoryModel getCategory() {
		return category;
	}


	/**
	 * @param category the category to set
	 */
	public void setCategory(CategoryModel category) {
		this.category = category;
	}
	
	/**
	 * 
	 * @param category
	 */
	public void setCategory(String category) {
		this.category.setId(category);
	}

	/**
	 * @return the owner
	 */
	public UserModel getOwner() {
		return owner;
	}


	/**
	 * @param owner the owner to set
	 */
	public void setOwner(UserModel owner) {
		this.owner = owner;
	}
	
	/**
	 * 
	 * @param owner
	 */
	public void setOwner(String owner) {
		this.owner.setId(owner);
	}


	/**
	 * @return the lastView
	 */
	public UserModel getLastView() {
		return lastView;
	}


	/**
	 * @param lastView the lastView to set
	 */
	public void setLastView(UserModel lastView) {
		this.lastView = lastView;
	}
	
	/**
	 * 
	 * @param lastView
	 */
	public void setLastView(String lastView) {
		this.lastView.setId(lastView);
	}


	/**
	 * @return the lastUpdate
	 */
	public UserModel getLastUpdate() {
		return lastUpdate;
	}


	/**
	 * @param lastUpdate the lastUpdate to set
	 */
	public void setLastUpdate(UserModel lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	/**
	 * @param lastUpdate the lastUpdate to set
	 */
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate.setId(lastUpdate);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the size
	 */
	public Integer getSize() {
		return size;
	}


	/**
	 * @param size the size to set
	 */
	public void setSize(Integer size) {
		this.size = size;
	}
	
	public void setSize(String strSize) {
		this.size = Integer.parseInt(strSize);
	}


	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}


	/**
	 * @param extension the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}


	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}


	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}


	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = Integer.parseInt(status);
	}


	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}


	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(String created) {
		this.created = getDateFromString(created);
	}
	
	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}


	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	/**
	 * 
	 * @param updated
	 */
	public void setUpdated(String updated) {
		this.updated = getDateFromString(updated);
	}

	public Object view() {
		HashMap<String, String> map = view("Id", getId());
		if(map!=null && !map.isEmpty() ) {
			super.setFieldValues(map);
			return map;
		}
		return null;
	}
	
	//make dynamic in base model
	public Object viewAll() {
		Object objs = null;
		
		if(!getDatabase().connect()){
			return null;
		}
		
		try {
			
			String sql = "SELECT * FROM "+getTableName();
			if(getCategory().getId() != null && !getCategory().getId().isEmpty()){
				sql += " WHERE category="+getCategory().getId();
			}
			sql +=";";
			getDatabase().select(sql);
			objs = viewAll(getDatabase().getResult()).toString();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			getDatabase().close();
		}
		
		return objs;
	}
}
