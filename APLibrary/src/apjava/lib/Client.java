package apjava.lib;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	//static reference to client
	private static Client client = null;
	private Socket socket;
	private BufferedOutputStream out;
	private BufferedInputStream in;
	private String hostname;
	private int port;
	private boolean isAlive;
	private static String token = "";
	
	private Client(String hostname, int port)
	{
		//set all values in client to null
		this.setAlive(true);
		this.in = null;
		this.out = null;
		this.socket = null;
		this.hostname = hostname;
		this.port = port;
	}
	
	
	public static Client getClient(String hostname, int port) {
		if(client == null || (client != null && !client.isAlive())) {
			client = new Client(hostname, port);
			try {
				client.connection();
				client.readyStreams();
			} catch (UnknownHostException e) {
				client.setAlive(false);
				e.printStackTrace();
			} catch (IOException e) {
				client.setAlive(false);
				e.printStackTrace();
			}
		}
		return  client;
	}
	
	/**
	 * @return the isAlive
	 */
	public boolean isAlive() {
		return isAlive;
	}

	/**
	 * @param isAlive the isAlive to set
	 */
	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public void connection() throws UnknownHostException, IOException{
		socket = new Socket(hostname, port);
	}
	
	public void readyStreams() throws IOException {
		out = new BufferedOutputStream(socket.getOutputStream());
		in = new BufferedInputStream(socket.getInputStream());
	}
	
	public void sendMessage(Message head, Object objBody)
	{
		try {
			//set token to head of message
			if(head.getToken().isEmpty()) {
				head.setToken(Client.token);
			}
			Middleware.sendMessage(head, objBody, socket, out);
		} catch (IOException e) {
			setAlive(false);
			e.printStackTrace();
		}
	}
	
	public Object[] recieveMessage() {
		
		Object [] objs = new Object[2];
		
		Message message = null;
		Object objBody = null;
		try {
			
			Object objReply = Middleware.readObj(socket);
			if(objReply != null) {
				
				if(objReply instanceof Message) {
					message = (Message)objReply;
					//set client token if none set as yet
					if(Client.token.isEmpty()) {
						Client.token = message.getToken();
					}
					objBody = "";
					while(!message.isReady()){
						objBody = Middleware.readObjBody(message, objBody, socket, in);
					}
				}
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			setAlive(false);
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		
		objs[0] = message;
		objs[1] = objBody;
		
		return objs;
	}
	
	public void close() {
		try {
			if(out != null) {
				out.close();
			}
			if(in != null) {
				in.close();
			}
			if(socket != null) {
				socket.close();
			}
			setAlive(false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
