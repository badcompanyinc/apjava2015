package apjava.server.view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class ServerView extends JFrame{
	public JFrame gui;
	public MainForm mainForm;
	public String input;
	
	private final static String newline = "\n";
	
	public ServerView(){
		super("Server");
		mainForm = new MainForm();
		getContentPane().add(mainForm.getPannel2(), BorderLayout.CENTER);
		getContentPane().add(mainForm.getPanel3(),BorderLayout.WEST);
		getContentPane().add(mainForm.getPannel(), BorderLayout.PAGE_END);
		//setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 500);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public MainForm getMainForm(){
		return mainForm;
	}
	
	public String getTextInput(){
		return getMainForm().getInputText().getText();
	}
	
	public void setInput(String input){
		this.input = input;
	}
	
	public String getInput(){ 
		return this.input;
	}
	
	public void sendText(final String text){ 
		SwingUtilities.invokeLater(
				new Runnable(){
					public void run(){
						getMainForm().getTextArea().append(text+newline);
					}
				}
		);
	}
	public void clearTextInput(){
		SwingUtilities.invokeLater(
				new Runnable(){
					public void run(){
						getMainForm().getInputText().setText("");
					}
				}
		);
	}
	
	public void clearForm()
	{
		SwingUtilities.invokeLater(
				new Runnable(){
					public void run(){
						getMainForm().getTextArea().setText("");
					}
				}
		);
	}
	
	public void send(){
		String text;
		text = getTextInput();
		if (text != null) {
			sendText(">>"+text);
		}
	}
}