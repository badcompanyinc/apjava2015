package apjava.client.controller;


public class AppHandler {
	private static ClientController activeController = null;
	public static UserClientController userClientController = null;
	public static FileClientController fileClientController = null;
	
	/**
	 * 
	 * @param clientController
	 * @param classForName
	 */
	public static void setMainViewVisible(String classForName) {
		
		if(activeController != null) {
			activeController.getMainView().dispose();
		}
		
		try {
			if(classForName.equals(UserClientController.class.getName())) {
				userClientController = UserClientController.class.newInstance();
				activeController = (ClientController) userClientController;
			}
			else if(classForName.equals(FileClientController.class.getName())) {
				fileClientController = FileClientController.class.newInstance();
				activeController = (ClientController) fileClientController;
			}
			activeController.getMainView().setVisible(true);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * 
	 * @param clientController
	 */
	public static void redirect(Class<?> classNameFor) {
		if(activeController != null && classNameFor.getName().equals(activeController.getClass().getName()))
		{
			return;
		}
		setMainViewVisible(classNameFor.getName());
	}
}
