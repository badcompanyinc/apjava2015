package apjava.client.controller;

import apjava.lib.Client;
import apjava.lib.Configuration;
import apjava.lib.Controller;
import apjava.lib.LookAndFeel;
import apjava.lib.Message;
import apjava.lib.gui.View;

public class ClientController extends Controller {
	private View mainView = null;
	
	
	public ClientController(){
		super();
		LookAndFeel.setLookAndFeel();
	}
	
	/**
	 * @return the mainView
	 */
	public View getMainView() {
		return mainView;
	}

	/**
	 * @param mainView the mainView to set
	 */
	public void setMainView(View mainView) {
		this.mainView = mainView;
	}

	public boolean sendMessage(Client client,Message message, Object msg) {
		if(client!=null) {
			client.sendMessage(message, msg);
		}
		return client.isAlive();
	}
	
	/**
	 * 
	 * @param controller
	 * @param action
	 * @param msg
	 * @param type
	 * @return boolean if message was sent
	 */
	public boolean sendMessage(Client client, String controller, String action, Object msg, String type)
	{
		Message message = new Message();
		message.setController(controller);
		message.setAction(action);
		message.setContentType(type);
		return sendMessage(client, message, msg);
	}
	
	public Object [] getReply(String controller, String action, Object msg, String type, int timeout)
	{
		Client client = Client.getClient(Configuration.server_hostname, Configuration.port);
		Object [] objs = null;
		//check if message was sent the try to get reply
		if(sendMessage(client,controller, action, msg, type)) {	
			long millis = System.currentTimeMillis();
			//place try block into while loop and end if error received
			while( ( System.currentTimeMillis() - millis) / 1000 < timeout) {
				if(( objs = client.recieveMessage()) != null && objs[0] != null) {
					break;
				}
			}
		}
		return objs;
	}
}
