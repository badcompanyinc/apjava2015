package apjava.lib.gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PopupContextMenuListener extends MouseAdapter {
	private View view;
	private int keypress;
	
	public PopupContextMenuListener(View view, int keypress) {
		super();
		this.view = view;
		this.keypress = keypress;
	}
	
    public void mousePressed(MouseEvent e){
        if (e.isPopupTrigger()) {
            doPop(e);
        } else if(e.getClickCount() == keypress) {
        	view.actionPerformed(null, e);
        }
    }

    public void mouseReleased(MouseEvent e){
        if (e.isPopupTrigger()) {
            doPop(e);
        }
    }

    private void doPop(MouseEvent e){
        DefaultContextMenu menu = new DefaultContextMenu();
        menu.show(e.getComponent(), e.getX(), e.getY());
        view.actionPerformed(menu,e);
    }
}
