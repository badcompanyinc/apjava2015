package apjava.client.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.filechooser.FileNameExtensionFilter;

import net.sf.image4j.codec.ico.ICODecoder;
import apjava.client.controller.AppHandler;
import apjava.client.controller.ClientController;
import apjava.client.controller.FileClientController;
import apjava.client.controller.UserClientController;
import apjava.lib.Configuration;
import apjava.lib.Helper;
import apjava.lib.gui.ChildDialog;
import apjava.lib.gui.DefaultContextMenu;
import apjava.lib.gui.DragUpdate;
import apjava.lib.gui.DropPane;
import apjava.lib.gui.DropTargetHandler;
import apjava.lib.gui.Form;
import apjava.lib.gui.JSystemFileChooser;
import apjava.lib.gui.PopupContextMenuListener;
import apjava.lib.gui.View;

@SuppressWarnings("serial")
public class FileView extends View {

	private PopupContextMenuListener popupMenu;

	private JTextField searchTxt;
	private JLabel categoryLbl;
	private JLabel userLbl;
	private JLabel lastViewLbl;
	private JLabel lastUpdateLbl;
	private JLabel nameLbl;
	private JLabel sizeLbl;
	private JLabel extensionLbl;

	private JLabel statusLbl;
	private JLabel createdLbl;
	// private JTable westtable;
	private JPanel middle;
	private JLabel doneby;

	private JScrollPane scroller;
	private JScrollPane scroller1;
	// private JLabel updatedLbl;
	private JLabel details;
	private JLabel category1Lbl;

	private JLabel lastView1Lbl;
	private JLabel name1Lbl;
	private JLabel size1Lbl;
	private JLabel extension1Lbl;
	private JLabel status1Lbl;
	private JLabel created1Lbl;

	private JButton logout;
	//private JButton gridView;
	//private JButton listView;
	private JButton uploadBtn;
	private JButton searchBtn;

	private JPanel north;
	private JPanel south;// WEST MAIN PANEL
	private JPanel west;
	private JPanel east;// EAST MAIN PANEL
	private JPanel center;
	private JPanel userpnl;
	private JPanel lastview;
	// private JPanel update;
	private JPanel detail;
	private JPanel ctgry;
	private JPanel buttonPane;
	private JPanel northright;
	private JPanel uploader;
	// private JPanel westcenter;
	private JPanel lower;
	private JPanel searcher;
	private JPanel searcher1;

	private JProgressBar progressBar;

	private String activeCategory;

	// determine if panel is highlighted
	private Component activePanel = null;
	
	//determine active category panel
	private JPanel activeCategoryPanel = null;

	private boolean firstCategory = true;

	public FileView(ClientController controller) {
		super("Group Drive", controller);
		popupMenu = new PopupContextMenuListener(this, 1);

		// setBackground(Color.decode("#CEA539"));

		((FileClientController) getController()).viewAllAction();
	}

	public JProgressBar getProgressBar() {
		return progressBar;
	}

	public void updateProgressBar(int val) {
		progressBar.setValue(val);
	}

	public void removeAllItems() {
		middle.removeAll();
		middle.revalidate();
		middle.repaint();
	}

	public void addFileItem(final Map<String, String> item) {
		middle.add(createItemTile(item));
		middle.validate();
		middle.repaint();
	}

	private JPanel createItemTile(Map<String, String> item) {

		JPanel panel = new JPanel(new BorderLayout());

		panel.addMouseListener(popupMenu);

		int length = (item.get("name").length() < 13) ? item.get("name")
				.length() : 13;
		String str = item.get("name").substring(0, length);
		JLabel label = new JLabel(str);

		JTextField idTF = new JTextField();
		idTF.setText(item.get("Id"));
		idTF.setVisible(false);

		JTextField nameTF = new JTextField();
		nameTF.setText(item.get("name"));
		nameTF.setVisible(false);

		JTextField sizeTF = new JTextField();
		sizeTF.setText(item.get("size"));
		sizeTF.setVisible(false);

		JTextField extensionTF = new JTextField();
		extensionTF.setText(item.get("extension"));
		extensionTF.setVisible(false);

		JTextField statusTF = new JTextField();
		statusTF.setText(item.get("status"));
		statusTF.setVisible(false);

		JTextField createdTF = new JTextField();
		createdTF.setText(item.get("created"));
		createdTF.setVisible(false);

		JTextField updatedTF = new JTextField();
		updatedTF.setText(item.get("updated"));
		updatedTF.setVisible(false);
		
		JTextField categoryTF = new JTextField();
		categoryTF.setText(item.get("category"));
		categoryTF.setVisible(false);

		panel.add(idTF);
		panel.add(nameTF);
		panel.add(sizeTF);
		panel.add(extensionTF);
		panel.add(statusTF);
		panel.add(createdTF);
		panel.add(updatedTF);
		panel.add(categoryTF);
		
		panel.add(label, BorderLayout.PAGE_END);
		panel.setBackground(Color.WHITE);
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		if (item.get("extension") != null) {
			String extension = item.get("extension").toLowerCase();
			BufferedImage img = null;
			ImageIcon icon = null;
			try {
				img = ImageIO.read(new File(Configuration.icons_cache_path
						+ extension + ".png"));
			} catch (IOException e1) {
				try {
					List<BufferedImage> images = ICODecoder.read(new File(
							"./icons/large/" + extension + ".ico"));
					File outputfile = new File("./icons/cache/" + extension
							+ ".png");
					ImageIO.write(images.get(0), "png", outputfile);
					img = images.get(0);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (img != null) {
				icon = new ImageIcon(img, "icon");
				JLabel iconLB = new JLabel(icon);
				panel.add(iconLB);
			} else {
				panel.add(Form.createBox(50, 50));
			}

		} else {
			panel.add(Form.createBox(50, 50));
		}
		return panel;
	}

	// north panel of border layout
	private void nothern() {
		searcher = new JPanel(new FlowLayout());
		ImageIcon img = new ImageIcon(FileView.class.getResource("/apjava/images/search.png"));
		
		searchBtn =new JButton( img);

		String userInfo = AppHandler.userClientController.getUserInfo();
		Map<String, String> map = Helper.convertToHashMap(userInfo);
		userLbl = new JLabel(map.get("userName"));
		final Component thisView = this;

		UserViewForm form = new UserViewForm();
		form.setFormData(map.get("firstName"), map.get("lastName"),
				map.get("userName"), map.get("email"));

		final UserViewForm formRef = form;

		userLbl.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent arg0) {

				ChildDialog dialog = ChildDialog.showDialog(thisView, formRef,
						userLbl, null, "User View", "Close");
				dialog.setSize(500, 400);
				dialog.getContentPane().setBackground(Color.WHITE);
				dialog.setVisible(true);
			}

		});
		searcher1=new JPanel();
		logout = new JButton("Logout");

		searchBtn.setPreferredSize(new Dimension(32,32));
		searchBtn.setMaximumSize(new Dimension(32,32));
		searchBtn.setMinimumSize(new Dimension(32,32));
		//searchBtn.setBackground(null);
		searchBtn.setBackground(Color.decode("#CEA539"));
		searchBtn.setOpaque(false);
		searchBtn.setBorder(null);
		
		/*logout.setPreferredSize(new Dimension(100, 20));
		logout.setMaximumSize(new Dimension(100, 20));
		logout.setMinimumSize(new Dimension(10, 20));
		//logout.setBackground(Color.decode("#CEA539"));*//**/

		searchTxt.setPreferredSize(new Dimension(465, 30));
		searchTxt.setMaximumSize(new Dimension(465, 30));
		searchTxt.setMinimumSize(new Dimension(465, 30));

		userLbl.setFont(new Font("Serif", Font.BOLD, 20));
		//userpnl.setBorder(BorderFactory.createTitledBorder( userLbl);
		//userpnl.setBorder(userLbl);
		
		userpnl.add(userLbl);		
		searcher1.add(searchTxt);		
		searcher1.add(searchBtn);
		searcher.add(searcher1);		
		north.add(logout, BorderLayout.EAST);
		northright.add(buttonPane, BorderLayout.CENTER);
		north.add(northright);
		north.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		north.add(userpnl, BorderLayout.WEST);
		// north.setBackground(Color.decode("#6BA5E7"));
	}

	// west region of border
	private void western() {

		west.setPreferredSize(new Dimension(180, 100));
		west.setMinimumSize(new Dimension(180, 100));
		west.setMaximumSize(new Dimension(180, 100));

		// westtable=new JTable();
		uploadBtn = Form.createSimpleButton("Upload");
		uploadBtn.setPreferredSize(new Dimension(170, 30));
		uploadBtn.setMaximumSize(new Dimension(170, 30));
		uploadBtn.setMinimumSize(new Dimension(170, 30));
		uploadBtn.setBackground(Color.decode("#CEA539"));

		scroller1 = new JScrollPane(ctgry);
		uploader.add(uploadBtn, BorderLayout.EAST);

		// westcenter.add(westtable);

		// westtable.setBackground(null);
		west.add(scroller1);
		west.add(uploader, BorderLayout.NORTH);
		west.add(ctgry, BorderLayout.CENTER);
		// west.add(westcenter, BorderLayout.CENTER);

	}

	public void addCategory(final Map<String, String> item) {
		JPanel panel = new JPanel();
		JPanel image=new JPanel(new FlowLayout());
	
		//ImageIcon im=new ImageIcon(FileView.class.getResource("/bg/search.png"));
		JLabel pic = new JLabel(new ImageIcon(FileView.class.getResource("/apjava/images/folder.png")));
		
		JButton btn = new JButton(item.get("name"));
		btn.setPreferredSize(new Dimension(100, 40));
		btn.setMaximumSize(new Dimension(100, 40));
		btn.setMinimumSize(new Dimension(100, 40));
		btn.setBackground(Color.WHITE);
		btn.setAlignmentY(LEFT_ALIGNMENT);
		btn.setOpaque(false);
		btn.setBorder(null);
		btn.setForeground(Color.BLACK);
		//btn.setName("buttonPretty");
		
		pic.setPreferredSize(new Dimension(32,32));
		pic.setMaximumSize(new Dimension(32,32));
		pic.setMinimumSize(new Dimension(32,32));
		//searchBtn.setBackground(null);
		pic.setBackground(Color.decode("#CEA539"));
		pic.setOpaque(false);
		pic.setBorder(null);
		

		// set action command to the id
		btn.setActionCommand(item.get("Id"));
		image.add(pic);
		image.add(btn);
		panel.add(image);
		
		final JPanel fpanel = panel;
		final JPanel fimage = image;
		final JButton fbtn = btn;
		
		if(firstCategory ) {
			activeCategoryPanel = panel;
			setupActiveCategoryPanel(fpanel, fimage, fbtn);
			firstCategory = false;
		}
		
		MouseListener mListener = new MouseListener(){
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(activeCategoryPanel != null){
					activeCategoryPanel.setBackground(Color.WHITE);
				}
				
				setupActiveCategoryPanel(fpanel, fimage, fbtn);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				fpanel.setBorder(BorderFactory.createLineBorder(Color.ORANGE));				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				fpanel.setBorder(BorderFactory.createLineBorder(Color.WHITE));
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				mouseClicked(arg0);
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
			}
		};
		
		panel.addMouseListener(mListener);
		image.addMouseListener(mListener);
		btn.addMouseListener(mListener);
		
		ActionListener listener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				JButton btn = (JButton) event.getSource();
				((FileClientController) getController()).viewAllAction(btn.getActionCommand());
				setActiveCategory(btn.getActionCommand());
			}
		};

		// add listener
		btn.addActionListener(listener);

		ctgry.add(panel);
		west.revalidate();
		west.repaint();
	}

	public void setupActiveCategoryPanel(JPanel fpanel, JPanel fimage, JButton fbtn) {
		activeCategoryPanel = fpanel;
		activeCategoryPanel.setBackground(Color.decode("#f29507"));
		fbtn.setBackground(Color.WHITE);
		fimage.setBackground(Color.WHITE);
		fbtn.setBackground(Color.WHITE);
		
	}

	protected void setActiveCategory(String actionCommand) {
		activeCategory = actionCommand;
	}

	// east region
	private void eastern() {

		detail.setLayout(new GridLayout(0, 1));
		detail.setPreferredSize(new Dimension(300, 200));
		detail.setMinimumSize(new Dimension(300, 200));
		detail.setMaximumSize(new Dimension(300, 200));

		details = new JLabel("FILE DETAILS");
		details.setFont(details.getFont().deriveFont(18.0f));

		nameLbl = new JLabel("<html><B>Name </B></html>");
		sizeLbl = new JLabel("<html><B>Size </B></html>");
		extensionLbl = new JLabel("<html><B>Extension</B></html>");
		statusLbl = new JLabel("<html><B>Status</B></html>");
		categoryLbl = new JLabel("<html><B>Category</B></html>");
		lastViewLbl = new JLabel("<html><B>Last Viewed</B></html>");
		lastUpdateLbl = new JLabel("<html><B>Last Updated</B></html>");
		createdLbl = new JLabel("<html><B>Date Created</B></html>");

		name1Lbl = new JLabel("NAME");
		size1Lbl = new JLabel("SIZE");
		extension1Lbl = new JLabel("EXTENSION");
		status1Lbl = new JLabel("STATUS");
		category1Lbl = new JLabel("CATEGORY");
		lastView1Lbl = new JLabel("LAST VIEWED");
		created1Lbl = new JLabel("CREATED");

		// ADD FIELDS TO WINDOW
		// lastview.add(details);

		detail.setBorder(BorderFactory.createEmptyBorder(20, 20, 0, 0));

		detail.add(details);
		detail.add(nameLbl);
		detail.add(name1Lbl);
		detail.add(sizeLbl);
		detail.add(size1Lbl);
		detail.add(extensionLbl);
		detail.add(extension1Lbl);
		detail.add(statusLbl);
		detail.add(status1Lbl);
		detail.add(categoryLbl);
		detail.add(category1Lbl);
		//detail.add(lastViewLbl);
		//detail.add(lastView1Lbl);
		detail.add(lastUpdateLbl);
		//detail.add(createdLbl);
		detail.add(created1Lbl);

		east.add(lastview, BorderLayout.NORTH);
		east.add(detail, BorderLayout.CENTER);// add to window
		east.setVisible(false);

	}

	// southern region
	private void southern() {
		doneby = new JLabel("Copyright � 2014-2015");
		lower.add(doneby);

		south.add(lower, BorderLayout.EAST);
	}

	// CENTER PANEL
	private void center() {
		// INITIALIZE
	//	gridView = new JButton("GridView");
		//listView = new JButton("ListView");

		middle = new DropPane(new DropTargetHandler() {
			@Override
			public void drop(DropTargetDropEvent dtde) {
				SwingUtilities.invokeLater(new DragUpdate((DropPane) this
						.getComponent(), false, null));

				Transferable transferable = dtde.getTransferable();
				if (dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
					dtde.acceptDrop(dtde.getDropAction());
					try {

						List transferData = (List) transferable.getTransferData(DataFlavor.javaFileListFlavor);
						if (transferData != null && transferData.size() > 0) {
							// ((DropPane)this.getComponent()).importFiles(transferData);
							((FileClientController) getController()).uploadAction(transferData, activeCategory);							
							dtde.dropComplete(true);
						}

					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} else {
					dtde.rejectDrop();
				}
			}

		});
		middle.setLayout(new FlowLayout(FlowLayout.LEFT));
		middle.setPreferredSize(new Dimension(100, 450));
		middle.setMinimumSize(new Dimension(100, 450));
		middle.setName("fileView.panel.middle");
		middle.setDropTarget(new DropTarget());
		

		scroller = new JScrollPane(middle);
		scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroller.setBorder(BorderFactory.createLineBorder(
				Color.decode("#cccccc"), 1));

		scroller.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				JScrollPane pane = (JScrollPane) (e.getSource());
				pane.revalidate();
			}
		});

		// progress bar
		progressBar = new JProgressBar(0, 100);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		progressBar.setVisible(false);
		// progressBar.setBorder(BorderFactory.createTitledBorder("Please wait"));

		// ADD TO PANEL
		buttonPane.add(progressBar);
		//buttonPane.add(gridView);
	//	buttonPane.add(listView);
		// buttonPane.setBackground(Color.decode("#6BA5E7"));

		center.add(scroller);
		northright.add(searcher, BorderLayout.EAST);
		//northright.add(buttonPane, BorderLayout.EAST);
		center.add(northright, BorderLayout.NORTH);

		// MODIFY PANEL
		middle.setBackground(Color.WHITE);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource().equals(uploadBtn)) {
			final JSystemFileChooser fc = new JSystemFileChooser();
			fc.setDialogTitle("Attach File To Upload");
			
			//fc.setAcceptAllFileFilterUsed(false);
			fc.addChoosableFileFilter(new FileNameExtensionFilter(".pdf", "pdf"));
			fc.addChoosableFileFilter(new FileNameExtensionFilter(".doc", "doc"));
			fc.addChoosableFileFilter(new FileNameExtensionFilter(".docx", "docx"));
			fc.addChoosableFileFilter(new FileNameExtensionFilter(".csv", "csv"));
			fc.addChoosableFileFilter(new FileNameExtensionFilter(".xls", "xls"));
			fc.addChoosableFileFilter(new FileNameExtensionFilter(".xlsx", "xlsx"));
			fc.addChoosableFileFilter(new FileNameExtensionFilter(".txt", "txt"));
			
			int option = fc.showOpenDialog(this);

			switch (option) {
			case JFileChooser.APPROVE_OPTION:
				((FileClientController) getController()).uploadAction(
						fc.getSelectedFile(), activeCategory);				
						//this.category1Lbl.setText(activeCategory);
				break;
			case JFileChooser.CANCEL_OPTION:
				break;
			case JFileChooser.ERROR_OPTION:
				break;
			}

		}else if (event.getSource().equals(logout)) {
			AppHandler.userClientController.logoutAction();
			AppHandler.redirect(UserClientController.class);
		} else if (event.getSource().equals(searchBtn)) {
			System.out.println("Databased Search function not implemented!");
		}

	}

	@Override
	public void initializeComponents() {
		north = new JPanel(new BorderLayout(10, 10));
		south = new JPanel(new BorderLayout(10, 10));// WEST MAIN PANEL
		west = new JPanel(new BorderLayout(10, 10));
		east = new JPanel(new BorderLayout(10, 10));// EAST MAIN PANEL
		center = new JPanel(new BorderLayout(10, 10));
		userpnl = new JPanel();
		lastview = new JPanel();
		searchTxt = new JTextField();
		searchTxt.setOpaque(false);
		// update=new JPanel();
		detail = new JPanel();
		ctgry = new JPanel(new FlowLayout());
		buttonPane = new JPanel();
		uploader = new JPanel();
		// westcenter=new JPanel();
		lower = new JPanel();
		northright = new JPanel(new BorderLayout());

		this.nothern();
		this.western();
		this.eastern();
		this.southern();
		this.center();

	}

	@Override
	public void addComponentsToPanels() {
		// TODO Auto-generated method stub

	}

	@Override
	public void registerListeners() {
		uploadBtn.addActionListener(this);
		//gridView.addActionListener(this);
		//listView.addActionListener(this);
		logout.addActionListener(this);
		searchBtn.addActionListener(this);
	}

	@Override
	public void addPanelsToFrame() {
		add(north, BorderLayout.NORTH); // adds to WINDOW
		add(west, BorderLayout.WEST); // ADDS TO WINDOW
		add(east, BorderLayout.EAST);
		add(south, BorderLayout.SOUTH);
		add(center, BorderLayout.CENTER);
	}

	@Override
	public void setWindowProperties() {
		super.setWindowProperties();
		setSize(1000, 600);
		setMinimumSize(new Dimension(780, 500));
		setMaximumSize(new Dimension(1000, 600));
		setLocationRelativeTo(null);
		// setExtendedState(JFrame.MAXIMIZED_BOTH);
		setResizable(false);
	}

	public void getPanelData(final MouseEvent e) {
		JPanel panel = (JPanel) e.getComponent();

		// JTextField idTF = (JTextField) panel.getComponent(0);
		JTextField nameTF = (JTextField) panel.getComponent(1);
		JTextField sizeTF = (JTextField) panel.getComponent(2);
		JTextField extensionTF = (JTextField) panel.getComponent(3);

		JTextField statusTF = (JTextField) panel.getComponent(4);
		JTextField createdTF = (JTextField) panel.getComponent(5);
		JTextField updatedTF = (JTextField) panel.getComponent(6);
		JTextField categoryTF = (JTextField) panel.getComponent(7);
				
		if(String.valueOf(statusTF.getText()).equals("0"))
			status1Lbl.setText("Not in Use");
		else
			status1Lbl.setText("In Use");
		
		name1Lbl.setText(nameTF.getText());
		size1Lbl.setText(sizeTF.getText());		
		extension1Lbl.setText(extensionTF.getText());
		//status1Lbl.setText(stat);
		created1Lbl.setText(createdTF.getText());
		lastView1Lbl.setText(updatedTF.getText());
		
		String categoryName = ((FileClientController) getController()).findCategoryById(String.valueOf(categoryTF.getText()));
		if(categoryName != null)
			category1Lbl.setText(categoryName);
		else
			category1Lbl.setText("unknown");
	}

	@Override
	public void actionPerformed(final DefaultContextMenu menu, final MouseEvent e) {
		final Component parent = this;
		ActionListener popup = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
 
				JPanel panel = (JPanel) e.getComponent();
				JTextField idTF = (JTextField) panel.getComponent(0);
				JTextField nameTF = (JTextField) panel.getComponent(1);

				if (arg0.getSource().equals(menu.getSelect())) {
					east.setVisible(true);
					getPanelData(e);
				} else if (arg0.getSource().equals(menu.getDelete())) {
					((FileClientController) getController()).deleteAction(
							panel, idTF.getText());
				} else if (arg0.getSource().equals(menu.getEdit())) {
					((FileClientController) getController()).editAction(
							idTF.getText(), nameTF.getText());
				} else if (arg0.getSource().equals(menu.getDownload())) {
					JSystemFileChooser fc = new JSystemFileChooser();
					fc.setSelectedFile(new File(nameTF.getText()));
					int option = fc.showSaveDialog(parent);
					
					switch (option) {
					case JFileChooser.APPROVE_OPTION:
						((FileClientController) getController())
										.downloadAction(fc.getSelectedFile()
										.getAbsolutePath(), idTF.getText());
						break;
					case JFileChooser.CANCEL_OPTION:
						break;
					case JFileChooser.ERROR_OPTION:
						break;
					}
				} else if (arg0.getSource().equals(menu.getPreview())) {
					JTextField updatedTF = (JTextField) panel.getComponent(6);
					((FileClientController) getController()).previewAction(
							idTF.getText(), nameTF.getText(), updatedTF.getText());
				}
			}

		};
		if (menu != null) {
			menu.getSelect().addActionListener(popup);
			menu.getDelete().addActionListener(popup);
			menu.getDownload().addActionListener(popup);
			menu.getEdit().addActionListener(popup);
			menu.getPreview().addActionListener(popup);
		} else if (e.getClickCount() == 1) {
			// change color when highlighted
			if (activePanel != null) {
				activePanel.setBackground(Color.white);
			}
			activePanel = (JPanel) e.getComponent();
			activePanel.setBackground(Color.decode("#FFFF99"));

			east.setVisible(true);
			getPanelData(e);
		} else if (e.getClickCount() == 2) {
			JPanel panel = (JPanel) e.getComponent();
			JTextField idTF = (JTextField) panel.getComponent(0);
			JTextField nameTF = (JTextField) panel.getComponent(1);
			JTextField updatedTF = (JTextField) panel.getComponent(6);
			((FileClientController) getController()).previewAction(
					idTF.getText(), nameTF.getText(), updatedTF.getText());
		}
	}

	public void removeItem(Component component) {
		middle.remove(component);
		middle.revalidate();
		middle.repaint();
	}

}
