package apjava.lib.gui;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

@SuppressWarnings("serial")
public class DefaultContextMenu extends JPopupMenu {
	private JMenuItem delete;
	private JMenuItem select;
	private JMenuItem download;
	private JMenuItem edit;
	private JMenuItem preview;
	
	private LookAndFeel old;

	public DefaultContextMenu() {
		delete = new JMenuItem("Delete");
		add(delete);
		add(new JSeparator());
		select = new JMenuItem("Select");
		add(select);
		add(new JSeparator());
		preview = new JMenuItem("Preview");
		add(preview);
		add(new JSeparator());
		edit = new JMenuItem("Edit");
		add(edit);
		add(new JSeparator());
		download = new JMenuItem("Download");
		add(download);
	}

	public void updateUI() {
		old = UIManager.getLookAndFeel();
		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Throwable ex) {
			old = null;
		}

		super.updateUI();
	}
	
	public void setVisible(boolean value){
		super.setVisible(value);
		if(value == false){
			if (old != null) {
				try {
					UIManager.setLookAndFeel(old);
				} catch (UnsupportedLookAndFeelException ignored) {
				}
			}			
		}
	}

	/**
	 * @return the delete
	 */
	public JMenuItem getDelete() {
		return delete;
	}

	/**
	 * @param delete
	 *            the delete to set
	 */
	public void setDelete(JMenuItem delete) {
		this.delete = delete;
	}

	/**
	 * @return the select
	 */
	public JMenuItem getSelect() {
		return select;
	}

	/**
	 * @param select
	 *            the select to set
	 */
	public void setSelect(JMenuItem select) {
		this.select = select;
	}

	/**
	 * @return the download
	 */
	public JMenuItem getDownload() {
		return download;
	}

	/**
	 * @param download
	 *            the download to set
	 */
	public void setDownload(JMenuItem download) {
		this.download = download;
	}

	public JMenuItem getEdit() {
		// TODO Auto-generated method stub
		return edit;
	}

	/**
	 * @return the preview
	 */
	public JMenuItem getPreview() {
		return preview;
	}

	/**
	 * @param preview the preview to set
	 */
	public void setPreview(JMenuItem preview) {
		this.preview = preview;
	}

}