package apjava.server.controller;

import apjava.lib.BaseModel;
import apjava.lib.Controller;

public abstract class ModelController extends Controller {
	private BaseModel model;

	/**
	 * @return the model
	 */
	public BaseModel getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(BaseModel model) {
		this.model = model;
	}
}
