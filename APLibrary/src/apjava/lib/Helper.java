package apjava.lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Helper {
	public static Map<String, String> convertToHashMap(String value) {
		Map<String,String> map = new HashMap<String, String>(0);  
		//check if {} are in the string
		if(value != null && !value.isEmpty() ) {
			if(value.charAt(0) == '{' && value.charAt(value.length()-1) == '}' ) {
				value = value.substring(1, value.length()-1);
			}
	
			String[] keyValuePairs = value.split(",");             
	
			if(keyValuePairs.length > 0)
			{
				for(String pair : keyValuePairs)
				{
				    String[] entry = pair.split("=");
				    if(entry.length == 2) {
				    	map.put(entry[0].trim(), entry[1].trim());
				    }
				}
			}
		}
		return map;
	}
	
	public static ArrayList<Map<String, String>> convertToArrayList(String value) {
		ArrayList<Map<String, String>> array = new ArrayList<Map<String, String>>(0);
		//check if [] are in the string
		if(value.charAt(0) == '[' && value.charAt(value.length()-1) == ']' ) {
			value = value.substring(1, value.length()-1);
		}
		
		Pattern pattern = Pattern.compile("\\s*(?<=\\}),\\s*");
		String[] values = pattern.split(value);
		
		for(String item : values) {
			array.add(convertToHashMap(item));
		}
		
		return array;
	}
	
	//TODO create this method to do the above by parsing the string in a loop
	public static ArrayList<Map<String, String>> parseToArrayList(String value) {
		return null;
	}
	
	/**
	 * 
	 * @param database
	 * @return
	 */
	public static boolean exists(String database){
		return new File(database).exists();
	}
	
	/**
	 * 
	 * @param fromFile
	 * @param toFile
	 * @throws IOException
	 */
    public static void copyFile(FileInputStream fromFile, FileOutputStream toFile) throws IOException {
        FileChannel fromChannel = null;
        FileChannel toChannel = null;
        try {
            fromChannel = fromFile.getChannel();
            toChannel = toFile.getChannel();
            fromChannel.transferTo(0, fromChannel.size(), toChannel);
        } finally {
            try {
                if (fromChannel != null) {
                    fromChannel.close();
                }
            } finally {
                if (toChannel != null) {
                    toChannel.close();
                }
            }
        }
    }
    
    public static boolean valueInArray(Object value, Object [] array){
    	for(Object obj : array){
    		if(obj.equals(value)){
    			return true;
    		}
    	}
    	return false;
    }
}
