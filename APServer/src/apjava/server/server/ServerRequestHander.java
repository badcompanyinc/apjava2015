package apjava.server.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;

import apjava.lib.Message;
import apjava.lib.Middleware;
import apjava.lib.Router;
import apjava.lib.Session;

public class ServerRequestHander implements Runnable{
	private int clientId;
	private Socket socket;
	private Server server;
	private BufferedOutputStream out;
	private BufferedInputStream reader;
	private boolean active;
	
	public ServerRequestHander(Server server, Socket socket, int clientId)
	{
		setServer(server);
		setClientId(clientId);
		setActive(true);
		try {
			out = new BufferedOutputStream(socket.getOutputStream());
			reader = new BufferedInputStream(socket.getInputStream());
		} catch (IOException e) {
			server.showException(e);
		}
		this.setSocket(socket);
	}
	
	public void closeStreamEditors() throws IOException {
		out.close();
		reader.close();
	}
	
	public Session getSession(Message message) {
		Session session = Session.getSession(message.getToken());
		return session;
	}
	
	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	
	/**
	 * 
	 * @param message
	 * @return
	 */
	private Message getResponseMessage(Message message)
	{
		try {
			return new Router().route(message, getSession(message));
		} catch (NoSuchMethodException e) {
			server.showException(e);
		} catch (SecurityException e) {
			server.showException(e);
		} catch (IllegalAccessException e) {
			server.showException(e);
		} catch (IllegalArgumentException e) {
			server.showException(e);
		} catch (InvocationTargetException e) {
			server.showException(e);
		} catch (ClassNotFoundException e) {
			server.showException(e);
		} catch (InstantiationException e) {
			server.showException(e);
		}
		Message rmessage = new Message();
		//an internal server error occured
		rmessage.setStatus(Message.error);
		return rmessage;
	}

	/**
	 * 
	 * @param message
	 * @param firstResponse
	 */
	public void response(Message message, boolean firstResponse){
		try {
			Message rmessage = getResponseMessage(message);
			Object flash = getSession(message).getValue(Session.flash);
			//set rmessage token for client if this is first response
			if(firstResponse) {
				rmessage.setToken(message.getToken());
			}
			Middleware.sendMessage(rmessage, flash , socket, out);
		} catch (IOException e) {
			server.showException(e);
		}
	}
	
	public void errorResponse() {
		try {
			Message rmessage = new Message();
			rmessage.setStatus(Message.error);
			rmessage.setContentType(Message.textContentType);
			rmessage.setErrorMessage(true);
			Middleware.sendMessage(rmessage, "error 505", socket, out);
		} catch (IOException e) {
			server.showException(e);
		}
	}
	
	public void request() throws ClassNotFoundException, IOException {
		
		if(socket.isClosed() || socket.getInputStream().available() < 0) {
			return;
		}
		
		Message message = null;
		Object objBody = null;
		Object obj = Middleware.readObj(socket);
		
		if(obj != null) {
			if(obj instanceof Message)
			{
				message = (Message)obj;
				server.getServerView().sendText("Request #"+this.clientId+": "+message.getController()+"/"+message.getAction()
						+" type: "+message.getContentType());
				
				objBody = "";
				
				while(!message.isReady() && !message.isErrorMessage()){
					objBody = Middleware.readObjBody(message, objBody, socket, reader);
				}
				
				boolean firstResponse = false;
				//find session if exit create if not
				Session session = getSession(message);
				if(!Session.getSessionMap().containsKey(message.getToken())) {
					message.setToken(session.getKey());
					firstResponse = true;
				}
				
				getSession(message).setKey("request.body", objBody);
				getSession(message).setKey("request.file", message.getFileInfo());
				response(message, firstResponse);
			}
		}
	}
	
	public void connectedClient()
	{
		server.getServerView().sendText("Accepted Client Request #:" + clientId + " IP Address: "+ socket.getInetAddress().getHostAddress());
	}
	
	@Override
	public void run(){
	    connectedClient();
	    
    	while(isActive() && server.isRunning()) {
    		try{
	    		request();
			} catch (ClassNotFoundException e) {
				server.showException(e);
				errorResponse();
				setActive(false);
			} catch (IOException e) {
				server.showException(e);
				setActive(false);
			}
    	}
    	
    	//set active to false
    	setActive(false);
    	
		try {
			this.closeStreamEditors();
			//this.interrupt(); //if is thread
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * @return the alive
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param alive the alive to set
	 */
	public void setActive(boolean alive) {
		this.active = alive;
	}

	/**
	 * @return the socket
	 */
	public Socket getSocket() {
		return socket;
	}

	/**
	 * @param socket the socket to set
	 */
	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	/**
	 * @return the server
	 */
	public Server getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(Server server) {
		this.server = server;
	}


}
