package apjava.lib;

import java.io.File;
import java.io.Serializable;
import java.util.Random;

@SuppressWarnings("serial")
public class FileInfo implements Serializable {
	
	private String originalName;
	private String temporaryName;
	private long length;
	private String extension;
	private String location;
	private String fileId;
	private String category;
	
	
	public FileInfo(String id, String originalName, String temporaryName, long length,
			String extension, String location, String category) {
		this.fileId = id;
		this.originalName = originalName;
		this.temporaryName = temporaryName;
		this.length = length;
		this.extension = extension;
		this.location = location;
		this.category = category;
	}
	
	public FileInfo(){
		setFileId("");
		setOriginalName("");
		setTemporaryName("");
		setLength(0);
		setExtension("");
		setLocation("");
		setCategory("");
	}
	
	public FileInfo(File file){
		setFileId("");
		setOriginalName(file.getName());
		setTemporaryName("");
		setLength(file.length());
		setLocation(file.getAbsolutePath());
		setExtension(findExtension(file.getName()));
		setCategory("");
	}
	
	public String findExtension(String fileName) {
		String extension="";
		int i=0;
		i = fileName.lastIndexOf('.');
		if(i>=0){
			extension=fileName.substring(i+1);
		}
		
		return extension;
	}
	/**
	 * @return the originalName
	 */
	public String getOriginalName() {
		return originalName;
	}
	/**
	 * @param originalName the originalName to set
	 */
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}
	/**
	 * @return the temporaryName
	 */
	public String getTemporaryName() {
		return temporaryName;
	}
	/**
	 * @param temporaryName the temporaryName to set
	 */
	public void setTemporaryName(String temporaryName) {
		this.temporaryName = temporaryName;
	}
	/**
	 * @return the length
	 */
	public long getLength() {
		return length;
	}
	/**
	 * @param length the length to set
	 */
	public void setLength(long length) {
		this.length = length;
	}
	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}
	/**
	 * @param extension the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if(category == null || category.isEmpty()){
			setCategory("");
		}
		return "Id=" + fileId + "&name=" + originalName + "&size=" + length + "&extension="
				+ extension + "&location=" + location+"&category="+category;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
