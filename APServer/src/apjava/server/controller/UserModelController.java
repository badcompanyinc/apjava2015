package apjava.server.controller;

import apjava.lib.Helper;
import apjava.lib.Message;
import apjava.lib.Session;
import apjava.server.model.UserModel;

public class UserModelController extends ModelController{
	
	public Message view(Session session) {
		UserModel model = new UserModel( "user_name="+Helper.convertToHashMap((String)session.getValue("authUser")).get("userName") );
		String value = model.view();
		if(value != null) {
			return session.flashOk(value);
		} else {
			return session.flashError("Could not find user");
		}
	}
	
	public Message create(Session session) {
		//save new user to the database using the user model
		UserModel model = new UserModel((String)session.getValue("request.body"));
		String error = model.save();
		if(error == null) {
			return session.flashOk("user created");
		} else {
			return session.flashError("Unable to create new user."+error);
		}
	}
	
	public Message login(Session session) {
		UserModel model = new UserModel((String)session.getValue("request.body"));
		if(session.getValue("authUser") != null) {
			return session.flashOk("already logged in!");
		}
		else 
			if(model.authenticate()) {
				session.setKey("authUser", model.view().toString() );
				return session.flashOk("user authenticated!");
			}
			else{
				return session.flashNotFound("Unable to authenticate user. Please try again!");
			}
	}
	
	public Message logout(Session session) {
		if(session.getValue("authUser") != null) {
			session.deleteKey("authUser");
			//delete session since user may not log in again
			//Session.getSessionMap().remove(session.getKey());
			return session.flashOk("logged out!");
		}
		return session.flashNotFound("not logged in!");
	}
}