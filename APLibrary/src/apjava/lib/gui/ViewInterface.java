package apjava.lib.gui;

import java.awt.event.ActionListener;

public interface ViewInterface extends ActionListener{
	public void initializeComponents();
	public void addComponentsToPanels();
	public void registerListeners();
}
