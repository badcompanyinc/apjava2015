package apjava.server.model;

import apjava.lib.BaseModel;
import apjava.lib.Configuration;
import apjava.lib.types.UniqueString;

public class CategoryModel extends BaseModel{
	
	private UniqueString name;
	

	public CategoryModel()
	{
		super(CategoryModel.class, Configuration.database_path+Configuration.databaseName,
				Configuration.databaseCommit);
		setName("");
	}

	public CategoryModel(String data) {
		super(CategoryModel.class, Configuration.database_path+Configuration.databaseName,
				Configuration.databaseCommit);
		super.setFieldValues(data);
	}
	
	public void addDefautCategories() {
		String oldName = getName();
		for(String str : Configuration.default_categories){
			setName(str);
			this.save();
		}
		setName(oldName);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name.getString();
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = new UniqueString(name);
	}

}
