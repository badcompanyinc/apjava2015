package apjava.lib;

import java.io.Serializable;

public class Message implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	public final static String notFound = "404";
	public final static String error = "500";
	public final static String ok = "200";

	public final static String textContentType ="text";
	public final static String fileContentType ="file";
	
	
	private String controller;
	private String action;
	//private String message;
	private String status;
	private String contentType;
	private String token;
	
	private long length;
	private boolean ready;
	private boolean errorMessage;
	
	private FileInfo fileInfo;
	
	public Message() {
		setStatus("");
		setController("");
		setAction("");
		setContentType("");
		setLength(0);
		setReady(false);
		setErrorMessage(false);
		setToken("");
		setFileInfo(null);
	}
	
	public Message(String status, String controller, String action, String type, long length) {
		setStatus(status);
		setController(controller);
		setAction(action);
		setContentType(type);
		setLength(length);
		setReady(false);
		setErrorMessage(false);
		setToken("");
		setFileInfo(null);
	}
	
	/**
	 * 
	 * @param msg
	 * @param status
	 * @return
	 */
	public static Message replyMessage(String status, String type) {
		Message message = new Message();
		message.setStatus(status);
		message.setContentType(type);
		return message;
	}
	
	/**
	 * 
	 * @param msg
	 * @return
	 */
	public static Message replyOk() {
		return replyMessage(ok, textContentType);
	}
	
	/**
	 * 
	 * @param msg
	 * @return
	 */
	public static Message replyNotFound(){
		return replyMessage(notFound, textContentType);
	}
	
	/**
	 * 
	 * @param msg
	 * @return
	 */
	public static Message replyError() {
		return replyMessage(error, textContentType);
	}
	
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the controller
	 */
	public String getController() {
		return controller;
	}

	/**
	 * @param controller the controller to set
	 */
	public void setController(String controller) {
		this.controller = controller;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the length
	 */
	public long getLength() {
		return length;
	}

	/**
	 * @param length the length to set
	 */
	public void setLength(long length) {
		this.length = length;
	}

	/**
	 * @return the ready
	 */
	public boolean isReady() {
		return ready;
	}

	/**
	 * @param ready the ready to set
	 */
	public void setReady(boolean ready) {
		this.ready = ready;
	}

	/**
	 * @return the errorMessage
	 */
	public boolean isErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(boolean errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the fileInfo
	 */
	public FileInfo getFileInfo() {
		return fileInfo;
	}

	/**
	 * @param fileInfo the fileInfo to set
	 */
	public void setFileInfo(FileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}
	
}
