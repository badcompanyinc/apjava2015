package apjava.lib;

public interface Model {
	public String save();
	public boolean update();
	public boolean delete();
	public Object viewAll();
	public Object view(String byColumn, String id);
}
