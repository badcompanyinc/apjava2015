package apjava.lib;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

public class Crypto {
	
	   /**
	    * From a password, a number of iterations and a salt,
	    * returns the corresponding digest
	    * @param iterationNb int The number of iterations of the algorithm
	    * @param password String The password to encrypt
	    * @param salt byte[] The salt
	    * @return byte[] The digested password
	    * @throws NoSuchAlgorithmException If the algorithm doesn't exist
	 * @throws UnsupportedEncodingException 
	    */
	   public static byte[] getHash(int iterationNb, String password, byte[] salt) throws NoSuchAlgorithmException, UnsupportedEncodingException {
	       MessageDigest digest = MessageDigest.getInstance("SHA-256");
	       digest.reset();
	       digest.update(salt);
	       byte[] input = digest.digest(password.getBytes("UTF-8"));
	       for (int i = 0; i < iterationNb; i++) {
	           digest.reset();
	           input = digest.digest(input);
	       }
	       return input;
	   }
	   /**
	    * From a base 64 representation, returns the corresponding byte[] 
	    * @param data String The base64 representation
	    * @return byte[]
	    */
	   public static byte[] base64ToByte(String data) {
	       return Base64.decode(data);
	   }
	 
	   
	   public static String randomMd5Hash() {
		   String currentTime = new Date(System.currentTimeMillis()).toString();
		   byte[] bytes = null;
		   MessageDigest md = null;
			try {
			   bytes = currentTime.getBytes("UTF-8");
			   md = MessageDigest.getInstance("MD5");
			   byte[] thedigest = md.digest(bytes);
			   return byteToBase64(thedigest);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
	   }
	   
	   /**
	    * From a byte[] returns a base 64 representation
	    * @param data byte[]
	    * @return String
	    */
	   public static String byteToBase64(byte[] data){
	       return Base64.encode(data);
	   }
	   
	   public static String hashPassword(String randomSalt, String password, int iterationNumber) throws NoSuchAlgorithmException, UnsupportedEncodingException
	   {
		   if(password == null) return null;
           // Salt generation 64 bits long
           byte [] bSalt = randomSalt.getBytes();
           // Digest computation
           byte[] bDigest = getHash(iterationNumber,password,bSalt);
           return byteToBase64(bDigest);
	   }
}
