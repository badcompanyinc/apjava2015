package apjava.client.view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import apjava.lib.gui.Form;

public class RegistrationForm extends Form{
	private JLabel firstNameLbl;
	private JLabel lastNameLbl;
	private JLabel emailLbl;
	private JLabel userNameLbl;
	private JLabel passwordLbl;
	
	private JTextField firstNameTxt;
	private JTextField lastNameTxt;
	private JTextField emailTxt;
	private JTextField userNameTxt;
	private JPasswordField passwordTxt;
	
	
	public RegistrationForm ()
	{
		//super();
		super(new FlowLayout(),"Registration Form", "Register", "Clear");
	}



	/**
	 * @return the firstNameLbl
	 */
	public JLabel getFirstNameLbl() {
		return firstNameLbl;
	}



	/**
	 * @param firstNameLbl the firstNameLbl to set
	 */
	public void setFirstNameLbl(JLabel firstNameLbl) {
		this.firstNameLbl = firstNameLbl;
	}



	/**
	 * @return the lastNameLbl
	 */
	public JLabel getLastNameLbl() {
		return lastNameLbl;
	}



	/**
	 * @param lastNameLbl the lastNameLbl to set
	 */
	public void setLastNameLbl(JLabel lastNameLbl) {
		this.lastNameLbl = lastNameLbl;
	}



	/**
	 * @return the emailLbl
	 */
	public JLabel getEmailLbl() {
		return emailLbl;
	}



	/**
	 * @param emailLbl the emailLbl to set
	 */
	public void setEmailLbl(JLabel emailLbl) {
		this.emailLbl = emailLbl;
	}



	/**
	 * @return the userNameLbl
	 */
	public JLabel getUserNameLbl() {
		return userNameLbl;
	}



	/**
	 * @param userNameLbl the userNameLbl to set
	 */
	public void setUserNameLbl(JLabel userNameLbl) {
		this.userNameLbl = userNameLbl;
	}



	/**
	 * @return the passwordLbl
	 */
	public JLabel getPasswordLbl() {
		return passwordLbl;
	}



	/**
	 * @param passwordLbl the passwordLbl to set
	 */
	public void setPasswordLbl(JLabel passwordLbl) {
		this.passwordLbl = passwordLbl;
	}



	/**
	 * @return the firstNameTxt
	 */
	public JTextField getFirstNameTxt() {
		return firstNameTxt;
	}



	/**
	 * @param firstNameTxt the firstNameTxt to set
	 */
	public void setFirstNameTxt(JTextField firstNameTxt) {
		this.firstNameTxt = firstNameTxt;
	}



	/**
	 * @return the lastNameTxt
	 */
	public JTextField getLastNameTxt() {
		return lastNameTxt;
	}



	/**
	 * @param lastNameTxt the lastNameTxt to set
	 */
	public void setLastNameTxt(JTextField lastNameTxt) {
		this.lastNameTxt = lastNameTxt;
	}



	/**
	 * @return the emailTxt
	 */
	public JTextField getEmailTxt() {
		return emailTxt;
	}



	/**
	 * @param emailTxt the emailTxt to set
	 */
	public void setEmailTxt(JTextField emailTxt) {
		this.emailTxt = emailTxt;
	}



	/**
	 * @return the userNameTxt
	 */
	public JTextField getUserNameTxt() {
		return userNameTxt;
	}



	/**
	 * @param userNameTxt the userNameTxt to set
	 */
	public void setUserNameTxt(JTextField userNameTxt) {
		this.userNameTxt = userNameTxt;
	}



	/**
	 * @return the passwordTxt
	 */
	public JPasswordField getPasswordTxt() {
		return passwordTxt;
	}



	/**
	 * @param passwordTxt the passwordTxt to set
	 */
	public void setPasswordTxt(JPasswordField passwordTxt) {
		this.passwordTxt = passwordTxt;
	}



	@Override
	public void initializeComponents() {
		// TODO Auto-generated method stub
		firstNameLbl=new JLabel("First Name");
		firstNameTxt = new JTextField(20);
		
		
		lastNameLbl =new JLabel("Last Name");
		lastNameTxt= new JTextField(20);
		
		
		emailLbl = new JLabel("Email");
		emailTxt = new JTextField(20);
		
		
		userNameLbl = new JLabel("User Name");
		userNameTxt= new JTextField(20);
		
		passwordLbl= new JLabel("Password");
		passwordTxt = new JPasswordField(20);
		
				
	}


	@Override
	public void addComponentsToPanels() {
		// TODO Auto-generated method stub
		
		
		add(firstNameLbl, firstNameTxt);
		add(lastNameLbl, lastNameTxt);
		add(emailLbl,emailTxt);
		add(userNameLbl,userNameTxt);
		add(passwordLbl, passwordTxt);
				
		getRootPanel().setBorder(emptyBorder(50, 100, 50, 100));
		
	}


	@Override
	public void registerListeners() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void clear() {
		this.getFirstNameTxt().setText("");
		this.getLastNameTxt().setText("");
		this.getUserNameTxt().setText("");
		this.getPasswordTxt().setText("");
		this.getEmailTxt().setText("");
		this.getMessageTF().setText("");
	}
	
}
