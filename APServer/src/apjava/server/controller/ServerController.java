package apjava.server.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Timer;

import javax.swing.JButton;
import javax.swing.JTextField;

import com.dropbox.core.DbxException;

import apjava.lib.Configuration;
import apjava.lib.Database;
import apjava.lib.DirWatcher;
import apjava.lib.DropboxSync;
import apjava.lib.FileWatcher;
import apjava.lib.Helper;
import apjava.server.model.CategoryModel;
import apjava.server.model.FileModel;
import apjava.server.model.UserModel;
import apjava.server.server.Server;
import apjava.server.view.ServerView;

public class ServerController extends ModelController {
	private Server server;
	private ServerView serverView;
	private Timer fileTimer;
	private Timer databaseFileTimer;
	private DropboxSync dropboxSync;

	public ServerController() {
		serverView = new ServerView();
		server = new Server(serverView, Configuration.port);
	
		try {
			serverView.sendText("Initializing Dropbox Please Wait ...");
			dropboxSync = new DropboxSync(Configuration.APP_KEY, Configuration.APP_SECRET,
					Configuration.APP_IDENTIFIER, Configuration.APP_ACCESS_TOKEN);
			serverView.sendText("Finished Initializing Dropbox");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DbxException e) {
			e.printStackTrace();
		}
		setActionListeners();
		initializeDirectories();
		initializeDatabase();
		initializeUploadFileWatcher();
		initializeDatabaseBackupFileWatcher();
	}

	private void initializeDatabaseBackupFileWatcher() {
		FileWatcher task = new FileWatcher(new File(Configuration.database_path + Configuration.databaseName)){
				@Override
				protected void onChange(File file) {
					try {
						//backup the database
						Database.importDatabase(file.getAbsolutePath(), Configuration.backup_database_path + 
								Configuration.databaseName);
						dropboxSync.saveFile("/"+file.getName(), file);
					} catch (IOException e) {
						e.printStackTrace();
					} catch (DbxException e) {
						e.printStackTrace();
					}
				}
		};
		databaseFileTimer = new Timer();
		// repeat the check every few second
		databaseFileTimer.schedule( task , new Date(), 1000 );		
	}

	private void initializeUploadFileWatcher() {
		DirWatcher task = new DirWatcher(Configuration.upload_path, null){
			@Override
			protected void onChange(File file, String action) {
				if(!action.equals(DirWatcher.deleteAction)){
					try {
						FileInputStream fromFile = new FileInputStream(file);
						FileOutputStream toFile = new FileOutputStream(Configuration.backup_file_path+file.getName());
						Helper.copyFile(fromFile, toFile);
						dropboxSync.saveFile("/"+file.getName(), file);
					} catch (IOException e) {
						e.printStackTrace();
					} catch (DbxException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					//delete the file
					new File(Configuration.backup_file_path+file.getName()).delete();
					//delete from dropbox
					try {
						dropboxSync.remove("/"+file.getName());
					} catch (DbxException e) {
						e.printStackTrace();
					}
				}
			}
		};
		fileTimer = new Timer();
		// repeat the check every few second
	    fileTimer.schedule( task , new Date(), 1000 );
	}

	private void initializeDirectories() {
		String[] directories = { Configuration.upload_path,
				Configuration.tmp_path, Configuration.database_path,
				Configuration.backup_database_path,
				Configuration.backup_file_path };

		for (String dir : directories) {
			if (new File(dir).mkdirs()) {
				serverView.sendText("Initializing Directory " + dir);
			}
		}
	}

	// initialize the database tables
	private void initializeDatabase() {
		String dbname = Configuration.databaseName;
		if (!Helper.exists(Configuration.database_path + dbname)) {
			if (Helper.exists(Configuration.backup_database_path + dbname)) {
				// call restore on a model
				serverView.sendText("Restoring Database and Files from Backup");
				new UserModel().restore(Configuration.backup_database_path + dbname, 
						Configuration.database_path + dbname);
				restoreBackupFiles();
			} else {
				serverView.sendText("Initializing Database");
				new CategoryModel().addDefautCategories();
				new FileModel();
				new UserModel();
				serverView.sendText("Finished Initializing Database");
			}
		}
	}

	private void restoreBackupFiles() {
		File [] files = new File(Configuration.backup_file_path).listFiles();
		for(File file : files){
			String newFile = Configuration.upload_path + file.getName();
			try {
				Helper.copyFile(new FileInputStream(file), new FileOutputStream(new File(newFile)));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void startServer() {
		if (!server.isRunning()) {
			// create a new server
			server = new Server(serverView, Configuration.port);
			server.startServer();
		} else {
			server.showException(new RuntimeException("Server already started!"));
		}
	}

	public void stopServer() {
		if (server.isRunning()) {
			serverView.sendText("Stopping server ...");
			server.stopServer();
			//stop the timer
			fileTimer.cancel(); 
			databaseFileTimer.cancel();
			try {
				server.getServerSocket().close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			server.showException(new RuntimeException("Server is not running!"));
		}
	}

	public void restartServer() throws IOException {
		stopServer();
		startServer();
	}

	public void helpServer() {
		String text = " -start - starts the server\n -stop - stops the server"
				+ "\n -restart - restarts the server\n -clear - clears the server";
		server.getServerView().sendText(text);
	}

	public void clearServer() {
		server.getServerView().clearForm();
	}

	public void route(String command) {
		// identify the method with that takes the argument of type Message
		Method method;
		try {
			method = this.getClass().getMethod(command + "Server");
			method.invoke(this);
		} catch (NoSuchMethodException e) {
			server.showException(e);
		} catch (SecurityException e) {
			server.showException(e);
		} catch (IllegalAccessException e) {
			server.showException(e);
		} catch (IllegalArgumentException e) {
			server.showException(e);
		} catch (InvocationTargetException e) {
			server.showException(e);
		}
	}

	// @Override
	public void setActionListeners() {
		// action listener for main form
		JButton submit = server.getServerView().getMainForm().getSubmit(); // get
																			// button
		JTextField textField = server.getServerView().getMainForm()
				.getInputText(); // get text that was inputed by user
		JButton start = server.getServerView().getMainForm().getStartserver();
		JButton stop = server.getServerView().getMainForm().getStopserver();
		JButton clear = server.getServerView().getMainForm().getClearserver();
		JButton restart = server.getServerView().getMainForm()
				.getRestartserver();

		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				server.getServerView().sendText(">>" + "start");
				route("start");
			}

		});

		stop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				server.getServerView().sendText(">>" + "stop");
				route("stop");
			}

		});

		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				server.getServerView().sendText(">>" + "clear");
				route("clear");
			}

		});

		restart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				server.getServerView().sendText(">>" + "restart");
				route("restart");
			}

		});

		submit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				server.getServerView().send();
				route(server.getServerView().getTextInput());
			}
		});

		textField.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				if (key == KeyEvent.VK_ENTER) {
					server.getServerView().send();
					route(server.getServerView().getTextInput());
				}
			}
		});
	}

}
