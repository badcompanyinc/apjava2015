package apjava.lib.gui;

import java.awt.Point;
import apjava.lib.gui.DropPane;

public class DragUpdate implements Runnable {

    private boolean dragOver;
    private Point dragPoint;
    private DropPane dropPane;

    public DragUpdate(DropPane dropPane, boolean dragOver, Point dragPoint) {
    	this.dropPane = dropPane;
        this.dragOver = dragOver;
        this.dragPoint = dragPoint;
    }

    @Override
    public void run() {
        dropPane.setDragOver(dragOver);
        dropPane.setDragOver(dragPoint);
        dropPane.repaint();
    }
}