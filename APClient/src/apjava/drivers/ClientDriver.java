package apjava.drivers;

import java.io.File;
import java.text.ParseException;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.synth.SynthLookAndFeel;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import apjava.client.controller.AppHandler;
import apjava.client.controller.UserClientController;
import apjava.lib.Configuration;

public class ClientDriver {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
		     public void run() {
		    	 initializeDirectories();
		    	 AppHandler.redirect(UserClientController.class);
		     }
		});
		//for DocumentEditor
		NativeInterface.open();
		NativeInterface.runEventPump();
	}
	
	private static void initializeDirectories() {
		String [] directories = {Configuration.tmp_path, Configuration.tmp_edit_path, Configuration.icons_cache_path };
		
		for(String dir : directories){
			if(new File(dir).mkdirs()){
				System.out.println("Initializing Directory "+dir);
			}
		}
	}
}
