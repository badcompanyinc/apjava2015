package apjava.lib;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;

import chrriis.dj.nativeswing.swtimpl.components.HTMLEditorAdapter;
import chrriis.dj.nativeswing.swtimpl.components.HTMLEditorSaveEvent;
import chrriis.dj.nativeswing.swtimpl.components.JHTMLEditor;

@SuppressWarnings("serial")
public abstract class CKEditor extends JPanel {

	protected static final String LS = System.getProperty("line.separator");
	private JHTMLEditor htmlEditor;
	public JPanel southPanel = new JPanel(new BorderLayout());
	public JPanel middlePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

	public CKEditor() {
		super(new BorderLayout());
		Map<String, String> optionMap = new HashMap<String, String>();
		optionMap
				.put("toolbar",
						"["
								//+ "  ['Source','-','Save','NewPage','Preview','-','Templates'],"
								+ "  ['Preview','-','Templates'],"
								+ "  ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],"
								+ "  ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],"
								+
								// "  ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],"
								// +
								"  '/',"
								//+ "  ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],"
								//+ "  ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],"
								//+ "  ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],"
								//+ "  ['Link','Unlink','Anchor'],"
								//+ "  ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],"
								+ "  '/',"
								+ "  ['Styles','Format','Font','FontSize'],"
								+ "  ['TextColor','BGColor'],"
								//+ "  ['Maximize', 'ShowBlocks','-','About']"
								+ "]");
		// optionMap.put("uiColor", "'#9AB8F3'");
		// optionMap.put("toolbarCanCollapse", "false");
		htmlEditor = new JHTMLEditor(
				JHTMLEditor.HTMLEditorImplementation.CKEditor,
				JHTMLEditor.CKEditorOptions.setOptions(optionMap));

		htmlEditor.addHTMLEditorListener(new HTMLEditorAdapter() {
			@Override
			public void saveHTML(HTMLEditorSaveEvent e) {
				saveContents();
			}
		});

		add(htmlEditor, BorderLayout.CENTER);
		southPanel.add(middlePanel, BorderLayout.NORTH);
		add(southPanel, BorderLayout.SOUTH);
	}

	protected abstract void saveContents();

	public void setContent(String content) {
		htmlEditor.setHTMLContent(content);
	}
	
	public String getContent(){
		return htmlEditor.getHTMLContent();
	}

}
