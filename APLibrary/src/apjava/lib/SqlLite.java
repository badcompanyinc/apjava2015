package apjava.lib;

import java.sql.*;

public class SqlLite {

	private Connection connection;
	private Statement statement;
	private String dbName;
	private boolean commit;
	private ResultSet result;
	
	public SqlLite(String dbName, boolean commit)
	{
		this.setDbName(dbName);
		this.setCommit(commit);
		this.setStatement(null);
		this.setResult(null);
	}
	
	/**
	 * @return the dbName
	 */
	public String getDbName() {
		return dbName;
	}

	/**
	 * @param dbName the dbName to set
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	/**
	 * @return the statement
	 */
	public Statement getStatement() {
		return statement;
	}

	/**
	 * @param statement the statement to set
	 */
	public void setStatement(Statement statement) {
		this.statement = statement;
	}

	/**
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * @param connection the connection to set
	 */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	/**
	 * @return the commit
	 */
	public boolean isCommit() {
		return commit;
	}

	/**
	 * @param commit the commit to set
	 */
	public void setCommit(boolean commit) {
		this.commit = commit;
	}
	
	
	/**
	 * @return the result
	 */
	public ResultSet getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(ResultSet result) {
		this.result = result;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean connect() {
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:"+dbName);
			//Database auto commit
			connection.setAutoCommit(isCommit());
			return true;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean exist() {			
		
		try {
			select("SELECT * FROM sqlite_sequence ;");
			ResultSet res = getResult();
			res.next();
			res.close();
			closeStatement();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}
	
	/**
	 * 
	 * @param sql
	 * @return
	 */
	public boolean createTable(String sql) {
		try {
			setStatement(connection.createStatement());
			getStatement().executeUpdate(sql);
			closeStatement();
			commit();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @param sql
	 * @return
	 * @throws SQLException 
	 */
	public void insert(String sql) throws SQLException {
		setStatement(connection.createStatement());
		getStatement().executeUpdate(sql);			
		closeStatement();
		commit();
	}
	
	/**
	 * 
	 * @param sql
	 * @return
	 * @throws SQLException 
	 */
	public void select(String sql) throws SQLException {
		setStatement(connection.createStatement());
		ResultSet result = getStatement().executeQuery(sql);
		setResult(result);	
	}
	
	/**
	 * 
	 * @param sql
	 * @return
	 * @throws SQLException 
	 */
	public void update(String sql) throws SQLException {
		setStatement(connection.createStatement());
		getStatement().executeUpdate(sql);
		closeStatement();
		commit();
	}
	
	/**
	 * 
	 * @param sql
	 * @return
	 * @throws SQLException 
	 */
	public void delete(String sql) throws SQLException {
		update(sql);
	}
	
	public void commit() throws SQLException {
		if(!isCommit()) {
			getConnection().commit();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean close() {
		try {
			getConnection().close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();			
		}
		return false;
	}
	
	/**
	 * 
	 * @param table
	 * @return
	 * @throws SQLException 
	 */
	public int getLastInsert(String table) throws SQLException {
		int id = 0;
		select("SELECT * FROM sqlite_sequence where name='"+table+"';");
		ResultSet res = getResult();
		res.next();
		id = res.getInt("seq");
		res.close();
		closeStatement();
		return id;
	}
	
	/**
	 * use after every select query to close statement
	 * @throws SQLException 
	 */
	public void closeStatement() throws SQLException{
		getStatement().close();
	}
	
	
}
