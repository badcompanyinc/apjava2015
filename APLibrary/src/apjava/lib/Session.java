package apjava.lib;

import java.util.HashMap;

public class Session {
	private static HashMap<String, Session> sessionMap = null;
	private HashMap<String,Object> session;
	private String key;
	
	public final static String flash = "flash";
	
	public Session() {
		session = new HashMap<String, Object>(0);
		//ensure that the key is unique
		do{
			key = Crypto.randomMd5Hash();
		}
		while(Session.getSessionMap().containsKey(key));
	}

	public static void initializeSessionMap() {
		Session.sessionMap = new HashMap<String, Session>(0);
	}
	
	public static Session getSession(String key) {
		if(Session.sessionMap.get(key) == null)	{
			Session session = new Session();
			Session.getSessionMap().put(session.getKey(), session);
			return session;
		}
		else {
			return Session.sessionMap.get(key);
		}
	}

	/**
	 * 
	 * @param msg
	 * @return
	 */
	public Message flashFileOk(Object msg) {
		setKey(flash, msg);
		Message message = Message.replyOk();
		message.setContentType(Message.fileContentType);
		return message;
	}
	
	/**
	 * 
	 * @param msg
	 * @return
	 */
	public Message flashOk(Object msg) {
		setKey(flash, msg);
		return Message.replyOk();
	}
	
	/**
	 * 
	 * @param msg
	 * @return
	 */
	public Message flashError(Object msg) {
		setKey(flash, msg);
		return Message.replyError();
	}
	

	public Message flashNotFound(Object msg) {
		setKey(flash, msg);
		return Message.replyNotFound();
	}
	
	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void setKey(String key, Object value) {
		session.put(key, value);
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public Object getValue(String key) {
		return session.get(key);
	}
	
	/**
	 * 
	 * @param key
	 */
	public void deleteKey(String key) {
		session.remove(key);
	}


	/**
	 * @return the sessionMap
	 */
	public static HashMap<String, Session> getSessionMap() {
		return sessionMap;
	}


	/**
	 * @param sessionMap the sessionMap to set
	 */
	public static void setSessionMap(HashMap<String, Session> sessionMap) {
		Session.sessionMap = sessionMap;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

}
