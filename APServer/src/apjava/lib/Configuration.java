package apjava.lib;

public class Configuration {
	
	/*
	 * The configuration
	 */
	public static final int port = 1354;
	public static String server_hostname = "127.0.0.1";
	public static final String databaseName = "apjava2015.sqlite";
	public static final boolean databaseCommit = true;	//sets auto commit to the database to true
	public static int ITERATION_NUMBER = 5;
	public static String randomSalt = "123sfsiefnkjn39u5934nksgnsk";
	public static String upload_path = "./uploads/";
	public static String tmp_path = "./tmp/";
	public static int threadPoolSize = 15;
	public static String database_path = "./database/";
	//backup file paths
	public static String backup_path = "./backup/";
	public static String backup_file_path = "./backup/files/";
	public static String backup_database_path = "./backup/database/";
	public static String [] default_categories = {"Finance", "Human Resource", "Athletics", "Health", "Auxiliary"};
	public static long timeAllowedToEdit = 5;
	//dropbox keys
	public static String APP_KEY = "frf4nf4nowtous2";
	public static String APP_SECRET = "a6s6m7x4050zpzp";
	public static String APP_IDENTIFIER = "APJava2015/1.0";
	public static String APP_ACCESS_TOKEN = "jy1dFT-nO4YAAAAAAAACKyHivOjYIF2BL-Q7crVeoUuTAeM9CmK-CxqCH1AVD3v_";
}
