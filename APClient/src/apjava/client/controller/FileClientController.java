package apjava.client.controller;

import java.awt.Component;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import apjava.client.view.FileView;
import apjava.lib.ClientFileHandler;
import apjava.lib.Configuration;
import apjava.lib.DocumentEditor;
import apjava.lib.FileInfo;
import apjava.lib.Helper;
import apjava.lib.Message;
import apjava.lib.Middleware;
import apjava.lib.gui.DeleteDialog;
import apjava.lib.gui.View;

public class FileClientController extends ClientController{

	private FileView fileView;
	protected ClientFileHandler clientFileHandler;
	private Map<String, String> categoryList;
	
	public FileClientController()
	{
		categoryList = new HashMap<String, String>(0);
		fileView = new FileView(this);
		setMainView(fileView);
	}

	public View getFileView() {
		return fileView;
	}

	
	private void uploadActionFuntion(File selectedFile, String category){
		FileInfo fileInfo = new FileInfo(selectedFile);
		if(Helper.valueInArray(fileInfo.getExtension(), Configuration.acceptedExtension)){
			fileInfo.setCategory(category);
			Object [] objs = getReply("File", "upload", fileInfo, Message.fileContentType, 30);
			if(objs != null) {
				Message msg = Middleware.getMessageFromObject(objs);
				if(msg!= null && msg.getStatus().equals(Message.ok) && objs[1] != null) {
					fileView.addFileItem(Helper.convertToHashMap((String)objs[1]));
				}
			}
		} else {
			JOptionPane.showMessageDialog(this.getMainView(), 
					"File Type for "+selectedFile.getName()+" not allowed", "Upload File", JOptionPane.ERROR_MESSAGE);					
		}		
	}
	
	//upload many files at once
	public void uploadAction(final List transferData, final String category) {
		Runnable runable =  new Runnable(){
			public void run(){
				for(Object fileObj : transferData.toArray()){
					uploadActionFuntion((File)fileObj, category);
				}
			}
		};
		(new Thread(runable)).start();		
	}
	
	public void uploadAction(final File selectedFile, final String category) {
		Runnable runable =  new Runnable(){
			public void run(){
				uploadActionFuntion((File)selectedFile, category);
			}
		};
		(new Thread(runable)).start();
	}
	
	//filter file results
	public void viewAllAction(final String catId) {
		Runnable runable = new Runnable(){
			public void run(){
				Object [] objs = getReply("File", "viewAll", catId, Message.textContentType, 30);
				
				if(objs != null) {
					Message msg = Middleware.getMessageFromObject(objs);
					if(msg!= null && msg.getStatus().equals(Message.ok)) {
						fileView.removeAllItems();
						ArrayList<Map<String,String>> list = Helper.convertToArrayList((String)objs[1]);
						int amt = list.size();
						if(amt > 0){
							int progress = 0;
							fileView.getProgressBar().setVisible(true);
							for(Map<String,String> item : list) {
								progress++;
								if(!item.isEmpty()){
									fileView.addFileItem(item);
								}
								fileView.updateProgressBar(Math.round(((float)progress/(float)amt) * 100));
							}
							fileView.getProgressBar().setVisible(false);
						}
					}
				}
			}
		};
		
		(new Thread(runable)).start();
	}
	
	public void viewAllAction() {
		Runnable runable = new Runnable(){
			public void run(){
				Object [] objs = getReply("File", "viewAll", "", Message.textContentType, 30);
				
				if(objs != null) {
					Message msg = Middleware.getMessageFromObject(objs);
					if(msg!= null && msg.getStatus().equals(Message.ok)) {
						fileView.removeAllItems();
						ArrayList<Map<String,String>> list = Helper.convertToArrayList((String)objs[1]);
						int amt = list.size();
						if(amt > 0){
							int progress = 0;
							fileView.getProgressBar().setVisible(true);
							for(Map<String,String> item : list) {
								progress++;
								if(!item.isEmpty()){
									fileView.addFileItem(item);
								}
								fileView.updateProgressBar(Math.round(((float)progress/(float)amt) * 100));
							}
							fileView.getProgressBar().setVisible(false);
						}
					}
				}
				//view All Categories
				viewAllCategories();
			}
		};
		
		(new Thread(runable)).start();
	}
	
	public void deleteAction(final Component component, final String id) {
		final FileClientController fileController = this;
		Runnable runable = new Runnable(){
			public void run(){
				DeleteDialog deleteDialog = new DeleteDialog(fileView, component);
				int value = ((Integer)deleteDialog.getResponse()).intValue();
				if (value == JOptionPane.YES_OPTION) {
					Object [] objs = getReply("File", "delete", id, Message.textContentType, 30);
					if(objs != null) {
						Message msg = Middleware.getMessageFromObject(objs);
						if(msg!= null && 
								(msg.getStatus().equals(Message.ok) || msg.getStatus().equals(Message.notFound))
								) {
							fileView.removeItem(component);
						} else {
							JOptionPane.showMessageDialog(fileController.getMainView(), 
									Middleware.getStringMessageBodyFromObject(objs), "Delete File", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		};
		
		(new Thread(runable)).start();
	}
	
	public void downloadAction(final String path, final String id) {
		final FileClientController fileController = this;
		Runnable runable =  new Runnable(){
			public void run(){
				Object [] objs = getReply("File", "download", id, Message.textContentType, 30);
				if(objs != null) {
					Message msg = Middleware.getMessageFromObject(objs);
					if(msg!= null && msg.getStatus().equals(Message.ok) && objs[1] instanceof File) {
						File file = (File)objs[1];
						if(file != null) {
							file.renameTo(new File(path));
						}
					} else {
						JOptionPane.showMessageDialog(fileController.getMainView(), 
								Middleware.getStringMessageBodyFromObject(objs), "Download File", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		};
		(new Thread(runable)).start();
	}

	public void viewAllCategories() {
		Object [] objs = getReply("Category", "viewAll", "", Message.textContentType, 30);
		if(objs != null) {
			Message msg = Middleware.getMessageFromObject(objs);
			if(msg!= null && msg.getStatus().equals(Message.ok)) {
				ArrayList<Map<String,String>> list = Helper.convertToArrayList((String)objs[1]);
				//add the all category
				Map<String, String> defaultItem = new HashMap<String, String>();
				defaultItem.put("name", "All");
				defaultItem.put("Id", "");
				fileView.addCategory(defaultItem);
				for(Map<String,String> item : list){
					if(!item.isEmpty()) {
						addToCurrentCategories(item);
						fileView.addCategory(item);
					}
				}
			}
		}
	}
	
	public String findCategoryById(String id){
		if(categoryList != null){
			if(categoryList.containsKey(id)) 
				return categoryList.get(id);
		}
		return null;
	}

	private void addToCurrentCategories(Map<String, String> item) {
		if(item != null)
		categoryList.put(item.get("Id"), item.get("name"));
	}

	public void editAction(final String id, final String name) {
		final FileClientController fileController = this;
		Runnable runable =  new Runnable(){
			public void run(){
				Object [] objs = getReply("File", "edit", id, Message.textContentType, 30);
				if(objs != null) {
					Message msg = Middleware.getMessageFromObject(objs);
					if(msg!= null && msg.getStatus().equals(Message.ok) && objs[1] != null) {
						File file = (File)objs[1];
						if(file != null) {
							String path = Configuration.tmp_edit_path+name;
							//remove old file with same name
							new File(Configuration.tmp_edit_path+name).delete();
							if(file.renameTo(new File(path))) {
								clientFileHandler = new ClientFileHandler(new File(path), fileController, id);
								clientFileHandler.start(1000);
								//TODO: comming soon
								//DocumentEditor.openFile(new File(path), fileController, id);
							}
						}
					} else {
						JOptionPane.showMessageDialog(fileController.getMainView(), 
								Middleware.getStringMessageBodyFromObject(objs), "Edit File", JOptionPane.INFORMATION_MESSAGE);
					}
				}
			}
		};
		(new Thread(runable)).start();
		
	}

	public void updateAction(final File file, final String fileId) {
		final FileClientController fileController = this;
		Runnable runable =  new Runnable(){
			public void run(){
				FileInfo fileInfo = new FileInfo(file);
				fileInfo.setFileId(fileId);
				Object [] objs = getReply("File", "update", fileInfo, Message.fileContentType, 30);
				if(objs != null) {
					Message msg = Middleware.getMessageFromObject(objs);
					if(msg!= null && msg.getStatus().equals(Message.ok) && objs[1] != null) {
						clientFileHandler.setChangePending(false);
						clientFileHandler.cancel();
						JOptionPane.showMessageDialog(fileController.getMainView(), 
								"File updated. Please close the file.", "update file", JOptionPane.INFORMATION_MESSAGE);
					} else {
						clientFileHandler.cancel();
						//remove the file
						//clientFileHandler.getFile().remove();
						JOptionPane.showMessageDialog(fileController.getMainView(), 
								Middleware.getStringMessageBodyFromObject(objs), "Update File", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		};
		(new Thread(runable)).start();
	}

	public void previewAction(final String id, final String name, final String updated) {
		final FileClientController fileController = this;
		Runnable runable =  new Runnable(){
			public void run(){
				Object [] objs = getReply("File", "download", id, Message.textContentType, 30);
				if(objs != null) {
					Message msg = Middleware.getMessageFromObject(objs);
					if(msg!= null && msg.getStatus().equals(Message.ok) && objs[1] != null) {
						File file = (File)objs[1];
						if(file != null) {
							String path = Configuration.tmp_edit_path+name;
							if(file.renameTo(new File(path))) {
								 DocumentEditor.openFile(new File(path), fileController, id, updated);
							} else if(new File(path).exists()) {
								DocumentEditor.openFile(new File(path), fileController, id, updated);
							}
						}
					} else {
						JOptionPane.showMessageDialog(fileController.getMainView(), 
								Middleware.getStringMessageBodyFromObject(objs), "Preview File", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		};
		(new Thread(runable)).start();
	}

	public String fileUpdatedAction(final String path, final String id, final String updated) {
		Object [] objs = getReply("File", "view", id, Message.textContentType, 30);
		if(objs != null) {
			Message msg = Middleware.getMessageFromObject(objs);
			Map<String, String> map = Helper.convertToHashMap((String)objs[1]);
			if(msg!= null && msg.getStatus().equals(Message.ok)) {
				if(!map.get("updated").equals(updated)){
					//fetch updated file
					Object [] objsFile = getReply("File", "download", id, Message.textContentType, 30);
					if(objs != null) {
						Message fileMsg = Middleware.getMessageFromObject(objs);
						if(fileMsg!= null && fileMsg.getStatus().equals(Message.ok) && objsFile[1] instanceof File) {
							File file = (File)objsFile[1];
							if(file != null) {
								try {
									Helper.copyFile(new FileInputStream(file), new FileOutputStream(new File(path)));
									file.delete();
									
									return map.get("updated");
								} catch (FileNotFoundException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						} else {
							System.out.println(Middleware.getStringMessageBodyFromObject(objs));
						}
					}
				}
			}
		}
		return null;
	}

}
