package apjava.lib.gui.synth.template;

import java.awt.Component;
import java.awt.Graphics;

public class RadioButtonOnIcon extends RadioButtonIcon {
	public RadioButtonOnIcon(int size) {
		super(size);
	}

	@Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		super.paintIcon(c, g, x, y);
		g.fillOval(x, y, getIconWidth() - 2, getIconHeight() - 2);
	}
}
