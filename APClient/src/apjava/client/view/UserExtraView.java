package apjava.client.view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JPanel;

import apjava.client.controller.UserClientController;
import apjava.lib.Controller;
import apjava.lib.gui.ViewInterface;


public class UserExtraView implements ViewInterface{
	
	private JButton regButton;
	private JButton loginButton;
	private JButton forgotButton;
	private JPanel rootPanel;
	private Controller controller;
	
	public UserExtraView(Controller controller)
	{		
		setController(controller);
		initializeComponents();
		addComponentsToPanels();
		registerListeners();
	}

	/**
	 * @return the controller
	 */
	public Controller getController() {
		return controller;
	}

	/**
	 * @param controller the controller to set
	 */
	public void setController(Controller controller) {
		this.controller = controller;
	}

	/**
	 * @return the forgotButton
	 */
	public JButton getForgotButton() {
		return forgotButton;
	}

	/**
	 * @param forgotButton the forgotButton to set
	 */
	public void setForgotButton(JButton forgotButton) {
		this.forgotButton = forgotButton;
	}

	/**
	 * @return the rootPanel
	 */
	public JPanel getRootPanel() {
		return rootPanel;
	}

	/**
	 * @param rootPanel the rootPanel to set
	 */
	public void setRootPanel(JPanel rootPanel) {
		this.rootPanel = rootPanel;
	}

	/**
	 * @return the loginButton
	 */
	public JButton getLoginButton() {
		return loginButton;
	}

	/**
	 * @param loginButton the loginButton to set
	 */
	public void setLoginButton(JButton loginButton) {
		this.loginButton = loginButton;
	}

	/**
	 * @return the regButton
	 */
	public JButton getRegButton() {
		return regButton;
	}

	/**
	 * @param regButton the regButton to set
	 */
	public void setRegButton(JButton regButton) {
		this.regButton = regButton;
	}

	@Override
	public void initializeComponents() {
		// TODO Auto-generated method stub
		setRegButton(new JButton("Register Account"));
		setLoginButton(new JButton("Login to Account"));
		setForgotButton(new JButton("Forgot Password"));
		setRootPanel(new JPanel(new FlowLayout()));
	}

	@Override
	public void addComponentsToPanels() {
		getRootPanel().add(getForgotButton());
		getRootPanel().add(getLoginButton());
		getRootPanel().add(getRegButton());
	}

	@Override
	public void registerListeners() {
		getForgotButton().addActionListener(this);
		getLoginButton().addActionListener(this);
		getRegButton().addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		UserClientController controller = (UserClientController)getController();
		JPanel rootRegistrationPanel = controller.getUserView().getRegistrationForm().getRootPanel();
		JPanel rootLoginPanel = controller.getUserView().getLoginForm().getRootPanel();
		
		if(e.getSource().equals(getLoginButton()))
		{	
			
			controller.getUserView().remove( rootRegistrationPanel );
			controller.getUserView().add( rootLoginPanel);//, BorderLayout.CENTER  );
			
			getLoginButton().setVisible(false);
			getRegButton().setVisible(true);
		}
		else
			if(e.getSource().equals(getRegButton()))
			{	
				controller.getUserView().remove( rootLoginPanel);
				controller.getUserView().add( rootRegistrationPanel);//, BorderLayout.CENTER );
				
				getLoginButton().setVisible(true);
				getRegButton().setVisible(false);
			}
		
		//repaint window
		controller.getUserView().revalidate();
		controller.getUserView().repaint();
	}

}
