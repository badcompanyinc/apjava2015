package apjava.lib.gui.synth.template;

import java.awt.*;

public class CheckBoxOnIcon extends CheckBoxIcon {

    public CheckBoxOnIcon(int size) {
        super(size);
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        super.paintIcon(c, g, x, y);
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(CustomPainter.THIN_STROKE);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.drawPolyline(new int[]{x, x + getIconWidth() / 2, x + getIconWidth()},
                new int[]{y + getIconWidth() / 2, y + getIconHeight(), y}, 3);
    }
}
