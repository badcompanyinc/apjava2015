package apjava.lib.gui;

import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;

import javax.swing.SwingUtilities;

public abstract class DropTargetHandler implements DropTargetListener {
	private Component component;
	
	public void setComponent(Component component){
		this.component = component;
	}

	public Component getComponent(){
		return this.component;
	}
	
	protected void processDrag(DropTargetDragEvent dtde) {
		if (dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
			dtde.acceptDrag(DnDConstants.ACTION_COPY);
		} else {
			dtde.rejectDrag();
		}
	}

	@Override
	public void dragEnter(DropTargetDragEvent dtde) {
		processDrag(dtde);
		SwingUtilities.invokeLater(new DragUpdate((DropPane) component, true, dtde.getLocation()));
		component.repaint();
	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) {
		processDrag(dtde);
		SwingUtilities.invokeLater(new DragUpdate((DropPane) component,true, dtde.getLocation()));
		component.repaint();
	}

	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {
	}

	@Override
	public void dragExit(DropTargetEvent dte) {
		SwingUtilities.invokeLater(new DragUpdate((DropPane) component,false, null));
		component.repaint();
	}

	
	public abstract void drop(DropTargetDropEvent dtde);
}
