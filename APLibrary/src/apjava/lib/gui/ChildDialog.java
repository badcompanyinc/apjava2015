package apjava.lib.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class ChildDialog extends JDialog implements ActionListener{
	
	public static ChildDialog dialog;
	public Form form;
	private ActionListener action;
	private JPanel jchild;
	private JPanel buttonPane;
	
	//manage active dialogs using arraylist
	private static ArrayList<Object[]> activeDialog = new ArrayList<Object[]>();

    /**
     * Set up and show the dialog.  The first Component argument
     * determines which frame the dialog depends on; it should be
     * a component in the dialog's controlling frame.
     */
    public static ChildDialog showDialog(Component frameComp,
            						Form child,
                                    Component locationComp,
                                    ActionListener action,
                                    String title,
                                    String longValue) {
        Frame frame = JOptionPane.getFrameForComponent(frameComp);
        dialog = new ChildDialog(frame,
        						child,
                                locationComp,
                                action,
                                title,
                                longValue);
        
        //add to activeDialog arrayList
        activeDialog.add(new Object[]{dialog, child});
        
        return dialog;
    }
    
    public static Object [] getVisible()
    {
    	//get last active dialog
    	int index = ChildDialog.activeDialog.size() - 1;
    	
    	return activeDialog.get(index);
    }
    
    /**
     * s
     * @param visible
     */
    public static void visible(boolean visible)
    {
    	//get last active dialog
    	int index = ChildDialog.activeDialog.size() - 1;
    	
    	if(index >=  0 && 
    			visible == false)
    		//remove from activeDialog arrayList
    		ChildDialog.dialog = (ChildDialog)activeDialog.remove(index)[0];
    	
    	ChildDialog.dialog.setVisible(visible);
    }

    private ChildDialog(Frame frame,
            		   Form child,
                       Component locationComp,
                       ActionListener action,
                       String title,
                       String longValue) {
        super(frame, title, true);

        this.setAction(action);
        
        if(longValue.isEmpty())
        	longValue = "Cancel";
        
        //Create and initialize the buttons.
        JButton cancelButton = new JButton(longValue);
        cancelButton.addActionListener(this);
        
        //final JButton actionButton = child.getButton();
        //actionButton.setActionCommand("Action");
        //actionButton.addActionListener(this);
        //getRootPane().setDefaultButton(actionButton);
        
        //Lay out the buttons from left to right.
        buttonPane = new JPanel();
        buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
        buttonPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        buttonPane.setAlignmentX(CENTER_ALIGNMENT);
        //buttonPane.add(Box.createHorizontalGlue());
        //buttonPane.add(Box.createRigidArea(new Dimension(20, 0)));
        //buttonPane.add(cancelButton);
        //buttonPane.add(Box.createRigidArea(new Dimension(10, 0)));
        //buttonPane.add(actionButton);
        
        //Put everything together, using the content pane's BorderLayout.
        setJchild(child.getRootPanel());
        Container contentPane = getContentPane();
        contentPane.add(getJchild(), BorderLayout.CENTER);
        contentPane.add(buttonPane, BorderLayout.PAGE_END);

        setResizable(false);
        //this.setLocationRelativeTo(frame);
        setLocationRelativeTo(locationComp);
    }
    
    //get the button pane
    public JPanel getbuttonPane()
    {
    	return buttonPane;
    }
    
    public void addButton(Component comp)
    {
    	getbuttonPane().add(comp);  
    }
	
    //Handle clicks on the Save and Cancel buttons.
    public void actionPerformed(ActionEvent e) {
        if ("Action".equals(e.getActionCommand())) {
            getAction().actionPerformed(e);
        }
        else //action was cancel hide dialog
        	ChildDialog.visible(false);
    }

	/**
	 * @return the action
	 */
	public ActionListener getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(ActionListener action) {
		this.action = action;
	}

	/**
	 * @return the jchild
	 */
	public JPanel getJchild() {
		return jchild;
	}

	/**
	 * @param jchild the jchild to set
	 */
	public void setJchild(JPanel jchild) {
		this.jchild = jchild;
	}
}
