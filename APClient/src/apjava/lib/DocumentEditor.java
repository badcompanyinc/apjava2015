package apjava.lib;

import java.awt.BorderLayout;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import chrriis.common.UIUtils;
import apjava.client.controller.FileClientController;
import docx4.ConvertOutHtml;

@SuppressWarnings("serial")
public class DocumentEditor extends CKEditor {
	private Controller controller;
	private File file;
	private String fileId;
	private String updated;

	DocumentEditor(File file, Controller controller, String fileId,
			String updated) {
		super();
		this.controller = controller;
		this.file = file;
		this.fileId = fileId;
		this.updated = updated;
		try {
			setContent(ConvertOutHtml.convertWordDoctoHTML(file));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void saveContents() {
		// ((FileClientController)controller).updateAction(file, fileId);
	}

	public void fileDatabaseMonitorTimer() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				String updatedR = ((FileClientController) controller).fileUpdatedAction(
						file.getAbsolutePath(), fileId, updated);
				if ( updatedR != null) {
					setContent(ConvertOutHtml.convertWordDoctoHTML(file));
					updated = updatedR;
				}
			}
		});
	}

	public void setTimerTask() {
		TimerTask timmer = new TimerTask() {
			@Override
			public void run() {
				fileDatabaseMonitorTimer();
			}

		};
		new Timer().schedule(timmer, 0, 1000);
	}

	public static void openFile(final File file, final Controller controller,
			final String fileId, final String updated) {
		UIUtils.setPreferredLookAndFeel();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame("Document File Editor");
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				DocumentEditor editor = new DocumentEditor(file, controller,
						fileId, updated);
				frame.getContentPane().add(editor, BorderLayout.CENTER);
				frame.setSize(800, 600);
				// frame.setLocationByPlatform(true);
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				editor.setTimerTask();
			}
		});
	}

}
