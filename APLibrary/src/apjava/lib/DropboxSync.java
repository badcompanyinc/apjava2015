package apjava.lib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxEntry;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuthNoRedirect;
import com.dropbox.core.DbxWriteMode;

public class DropboxSync {
	
	private DbxClient client;
	
    public DropboxSync(String APP_KEY, String APP_SECRET, String appIdentifier, String accessToken) throws IOException, DbxException {
        DbxAppInfo appInfo = new DbxAppInfo(APP_KEY, APP_SECRET);

        DbxRequestConfig config = new DbxRequestConfig(appIdentifier,
            Locale.getDefault().toString());
        
    	if(accessToken == null){ //get access token if null
	        DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config, appInfo);
	        // Have the user sign in and authorize your app.
	        String authorizeUrl = webAuth.start();
	        System.out.println("1. Go to: " + authorizeUrl);
	        System.out.println("2. Click \"Allow\" (you might have to log in first)");
	        System.out.println("3. Copy the authorization code.");
	        String code = new BufferedReader(new InputStreamReader(System.in)).readLine().trim();
	        // This will fail if the user enters an invalid authorization code.
	        DbxAuthFinish authFinish = webAuth.finish(code);
	        accessToken = authFinish.accessToken;
    	}

        client = new DbxClient(config, accessToken);

        System.out.println("Linked account: " + client.getAccountInfo().displayName);
    }
    
    
    public DbxEntry.WithChildren listing() throws DbxException{
    	return client.getMetadataWithChildren("/");
    }
    
    
    public DbxEntry.File download(String name, FileOutputStream outputStream) throws DbxException, IOException{
        try {
            DbxEntry.File downloadedFile = client.getFile(name, null,
                outputStream);
            return downloadedFile;
        } finally {
            outputStream.close();
        }
    }

    public void remove(String name) throws DbxException {
        client.delete(name);	
    }
    
    public DbxEntry.File upload(String name, File inputFile) throws DbxException, IOException{
        FileInputStream inputStream = new FileInputStream(inputFile);
        try {
            DbxEntry.File uploadedFile = client.uploadFile(name, DbxWriteMode.add(), inputFile.length(), inputStream);
            return uploadedFile;
        } finally {
            inputStream.close();
        }   	
    }
    
    public DbxEntry.File modify(String name, String revisionToReplace, File inputFile) throws DbxException, IOException{
        FileInputStream inputStream = new FileInputStream(inputFile);
        try {
        	
            DbxEntry.File uploadedFile = client.uploadFile(name, DbxWriteMode.update(revisionToReplace), inputFile.length(), inputStream);
            return uploadedFile;
        } finally {
            inputStream.close();
        }   	
    }
    
    
    public DbxEntry.File saveFile(String name, File inputFile) throws DbxException, IOException{
    	DbxEntry.File file = exists(name);
        if(file != null){
        	return modify(name, file.rev, inputFile);
        }
        return upload(name, inputFile);
    }
    
    public DbxEntry.File exists(String name){
    	try {
    		DbxEntry file = client.getMetadata(name);
    		if(file != null)
    			return file.asFile();
		} catch (DbxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }

}
