package apjava.lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public abstract class Database {
	
	private SqlLite database;

	public Database(String name, boolean commit)
	{
		setDatabase(new SqlLite(name, commit));
	}

	/**
	 * @return the database
	 */
	public SqlLite getDatabase() {
		return database;
	}

	/**
	 * @param database the database to set
	 */
	public void setDatabase(SqlLite database) {
		this.database = database;
	}
	
	/**
	 * 
	 */
	public void connect()
	{
		getDatabase().connect();
	}
	
	/**
	 * @param sql the sql statement for database table
	 */
	public void create(String sql)
	{
		getDatabase().createTable(sql);
	}
	
	public static boolean importDatabase(String fromPath, String toPath) throws IOException {

	    File newDb = new File(toPath);
	    File oldDb = new File(fromPath);
	    if (oldDb.exists()) {
	        Helper.copyFile(new FileInputStream(oldDb), new FileOutputStream(newDb));
	        return true;
	    }
	    return false;
	}
	
	/**
	 * Backup the database to appropriate path
	 */
	public void backup(String databaseFile, String backupDatabaseFile){
		try {
			importDatabase(databaseFile, 
					backupDatabaseFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Restore the database from appropriate path
	 */
	public void restore(String databaseFile, String backupDatabaseFile){
		try {
			importDatabase(databaseFile, backupDatabaseFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	/**
	 * @param str
	 * @return
	 */
	protected static String coats(String str)
	{
		return "'"+str+"'";
	}
}
