package apjava.lib;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Timer;

import javax.swing.JOptionPane;

import apjava.client.controller.ClientController;
import apjava.client.controller.FileClientController;
import apjava.lib.FileWatcher;

public class ClientFileHandler extends FileWatcher{

	private ClientController controller;
	private String fileId;
	private Timer timer = new Timer();
	private boolean changePending = false;
	
	public ClientFileHandler(File file, ClientController controller, String id) {
		super(file);
		this.controller = controller;
		this.setFileId(id);
		//open the current file in edit mode
		if (Desktop.isDesktopSupported()) {
		    try {
				Desktop.getDesktop().edit(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
		    //not supported
		}
	}

	@Override
	protected void onChange(File file) {
		if(changePending == false){
			changePending = true;
			int val = JOptionPane.showConfirmDialog(this.controller.getMainView(), "Are you finish updating "+file.getName()+"?", "update file", JOptionPane.OK_CANCEL_OPTION);
			if(val == JOptionPane.OK_OPTION) {
				((FileClientController)this.controller).updateAction(file, fileId);
			}
		}
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	
	public void setChangePending(boolean value){
		this.changePending = value;
	}
	
	public void stop(){
		timer.cancel();
	}
	
	public void start(long time){
	    // repeat the check every second
	    timer.schedule( this , new Date(), time );
	}

}
