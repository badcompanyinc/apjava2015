package docx4;

import java.io.File;
import java.io.IOException;

import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;



public class ConvertOutHtml{

	public static String convertWordDoctoHTML(File file){
		/*
		FileInfo fileInfo = new FileInfo(file);
		
		OutputStream out = new ByteArrayOutputStream();

		try {
			InputStream in= new FileInputStream(file);
			if(fileInfo.getExtension().equals("docx") ||fileInfo.getExtension().equals("doc") ) {
				XWPFDocument document;
				document = new XWPFDocument(in);
				XHTMLOptions options = XHTMLOptions.create().URIResolver(new FileURIResolver(new File(file.getAbsolutePath()+"_media")));
				XHTMLConverter.getInstance().convert(document, out, options);
				return out.toString();
			}
			else if(fileInfo.getExtension().equals("txt")) {
				String txt = "", line;
				BufferedReader reader = new BufferedReader(new FileReader(file));
			    while ((line = reader.readLine()) != null) {
			        txt+= line+"<br>";
			    }
			    return txt;
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		*/
		try {
			return new ParseDocument().parseToHTML(file);
		} catch (IOException | SAXException | TikaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	

}
