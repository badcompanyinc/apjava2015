package apjava.lib.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

public abstract class Form implements ViewInterface {
	private JPanel rootPanel;
	private JPanel buttonPanel;
	private JPanel labelPanel;
	private JPanel fieldPanel;
	private JLabel messageLB;
	private JButton submitBtn;
	private JButton clearBtn;
	private JLabel title;
	
	public static String default_submit_btn = "Submit";
	public static String default_clear_btn="clear";

	public Form(String title) {
		setTitle(new JLabel(title));
		setButtonPanel(new JPanel());
		setRootPanel(new JPanel());
		setFieldPanel(new JPanel());
		setLabelPanel(new JPanel());
		setSubmitBtn(createSimpleButton(default_submit_btn));
		setClearBtn(createSimpleButton(default_clear_btn));

		initializeComponents();
		setLayouts();
		beforeAddComponentsToPanels();
		addComponentsToPanels();
		afterAddComponentsToPanels();
		registerListeners();
	}

	public Form(LayoutManager layout, String title, String submitBtnTxt,
			String clearBtnTxt) {
		setTitle(new JLabel(title));
		setButtonPanel(new JPanel());
		setRootPanel(new JPanel());
		setFieldPanel(new JPanel());
		setLabelPanel(new JPanel());
		setClearBtn(createSimpleButton(clearBtnTxt));
		setSubmitBtn(createSimpleButton(submitBtnTxt));

		initializeComponents();
		setLayouts();
		beforeAddComponentsToPanels();
		addComponentsToPanels();
		afterAddComponentsToPanels();
		registerListeners();
	}

	/**
	 * @return the title
	 */
	public JLabel getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(JLabel title) {
		this.title = title;
		title.setName("title");
	}

	public void addToFieldPanel(Component comp) {
		fieldPanel.add(comp);
	}

	public void addToLabelPanel(Component comp) {
		labelPanel.add(comp);
	}

	public void add(Component comp, Component comp2) {
		addToLabelPanel(comp);
		addToFieldPanel(comp2);
	}

	/**
	 * 
	 */
	private void beforeAddComponentsToPanels() {
		setMessageTF(new JLabel());
		getTitle().setAlignmentX(JLabel.CENTER);
		add(createBox(10, 10), getTitle());
		add(createBox(10, 10), getMessageTF());
	}

	/**
	 * 
	 */
	private void afterAddComponentsToPanels() {
		getRootPanel().add(labelPanel, BorderLayout.WEST);
		getRootPanel().add(fieldPanel, BorderLayout.CENTER);
		getButtonPanel().add(getClearBtn());
		getButtonPanel().add(getSubmitBtn());
		add(createBox(10, 10), getButtonPanel());
	}

	/**
	 * 
	 * @param panel
	 * @param layout
	 */
	public void setLayouts() {
		getButtonPanel().setLayout(new GridLayout(1, 1, 10, 0));
		getRootPanel().setLayout(new BorderLayout(10, 10));
		getFieldPanel().setLayout(new GridLayout(0, 1, 0, 10));
		getLabelPanel().setLayout(new GridLayout(0, 1, 0, 10));
	}

	/**
	 * @return the rootPannel
	 */
	public JPanel getRootPanel() {
		return rootPanel;
	}

	/**
	 * @param rootPannel
	 *            the rootPannel to set
	 */
	public void setRootPanel(JPanel rootPanel) {
		this.rootPanel = rootPanel;
	}

	/**
	 * 
	 * @param top
	 * @param left
	 * @param bottom
	 * @param right
	 * @return
	 */
	public static Border emptyBorder(int top, int left, int bottom, int right) {
		return BorderFactory.createEmptyBorder(top, left, bottom, right);
	}

	/**
	 * 
	 * @param width
	 * @param height
	 * @return
	 */
	public static Component createBox(int width, int height) {
		return Box.createRigidArea(new Dimension(width, height));
	}

	/**
	 * @return the submitBtn
	 */
	public JButton getSubmitBtn() {
		return submitBtn;
	}

	/**
	 * @param submitBtn
	 *            the submitBtn to set
	 */
	public void setSubmitBtn(JButton submitBtn) {
		this.submitBtn = submitBtn;
	}

	/**
	 * @return the clearBtn
	 */
	public JButton getClearBtn() {
		return clearBtn;
	}

	/**
	 * @param clearBtn
	 *            the clearBtn to set
	 */
	public void setClearBtn(JButton clearBtn) {
		this.clearBtn = clearBtn;
	}

	/**
	 * @return the messageTF
	 */
	public JLabel getMessageTF() {
		return messageLB;
	}

	/**
	 * @param messageTF
	 *            the messageTF to set
	 */
	public void setMessageTF(JLabel messageLB) {
		this.messageLB = messageLB;
	}

	/**
	 * @return the labelPanel
	 */
	public JPanel getLabelPanel() {
		return labelPanel;
	}

	/**
	 * @param labelPanel
	 *            the labelPanel to set
	 */
	public void setLabelPanel(JPanel labelPanel) {
		this.labelPanel = labelPanel;
	}

	/**
	 * @return the fieldPanel
	 */
	public JPanel getFieldPanel() {
		return fieldPanel;
	}

	/**
	 * @param fieldPanel
	 *            the fieldPanel to set
	 */
	public void setFieldPanel(JPanel fieldPanel) {
		this.fieldPanel = fieldPanel;
	}

	/**
	 * @return the buttonPanel
	 */
	public JPanel getButtonPanel() {
		return buttonPanel;
	}

	/**
	 * @param buttonPanel
	 *            the buttonPanel to set
	 */
	public void setButtonPanel(JPanel buttonPanel) {
		this.buttonPanel = buttonPanel;
	}

	public abstract void clear();

	public static void disableButton(JButton button) {
		button.setEnabled(false);
	}

	public static void enableButton(JButton button) {
		button.setEnabled(true);
	}

	public static JButton createSimpleButton(String text) {
		JButton button = new JButton(text);
		button.setForeground(Color.WHITE);
		button.setBackground(Color.decode("#BF1E4B"));
		button.setName("buttonPretty");
		return button;
	}
}
