package apjava.lib.gui.synth.template;

import javax.swing.*;
import javax.swing.plaf.synth.ColorType;
import javax.swing.plaf.synth.SynthConstants;
import javax.swing.plaf.synth.SynthContext;
import javax.swing.plaf.synth.SynthPainter;

import java.awt.*;

public class CustomPainter extends SynthPainter {

	/** Background image for panels. */
	@SuppressWarnings("unused")
	private final Image background = null;
	static final Stroke THIN_STROKE = new BasicStroke(1);

	public CustomPainter() {//throws IOException {
	    //this.background = ImageIO.read(apjava.drivers.ClientDriver.class.getResource("../images/background.png"));
	}

    private void paintBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        Graphics2D g2 = (Graphics2D)g;

        final boolean isFocused = (context.getComponentState() & SynthConstants.FOCUSED) > 0;

        g2.setColor(context.getStyle().getColor(context, ColorType.BACKGROUND));
        g2.fillRect(x + 1, y + 1, w - 3, h - 3);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g2.setColor(isFocused ? context.getStyle().getColor(context, ColorType.FOCUS) : context.getStyle().getColor(context, ColorType.FOREGROUND));
        g2.setStroke(THIN_STROKE);
        g2.drawRect(x, y, w - 1, h - 1);
    }
    
    private void paintEmptyBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        Graphics2D g2 = (Graphics2D)g;

        final boolean isFocused = (context.getComponentState() & SynthConstants.FOCUSED) > 0;

        g2.setColor(context.getStyle().getColor(context, ColorType.BACKGROUND));
        g2.fillRect(x, y, w, h);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g2.setColor(isFocused ? context.getStyle().getColor(context, ColorType.FOCUS) : context.getStyle().getColor(context, ColorType.FOREGROUND));
        g2.setStroke(THIN_STROKE);
    }

	/** Paint the component using a gradient based on the two provided colors. */
    public static void paintVerticalGradient(Graphics g, int x, int y, int w, int h, Color fg, Color bg) {
        Graphics2D g2 = (Graphics2D)g;
        //g2.setPaint(getGradient(x, y, h, fg, bg));
        g2.fillRect(x, y, w, h);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(fg);
        g2.setStroke(THIN_STROKE);
        g2.drawRect(x, y, w, h);
    }

    public static GradientPaint getGradient(int x, int y, int h, Color fg, Color bg) {
        return new GradientPaint(x, y, bg, x, y + h, fg);
    }

	@SuppressWarnings("unused")
	private Color mix(Color a, Color b) {
		return new Color((a.getRed() + b.getRed()) / 2,
				(a.getGreen() + b.getGreen()) / 2,
				(a.getBlue() + b.getBlue()) / 2,
				(a.getAlpha() + b.getAlpha()) / 2);
	}

	private void paintVerticalGradient(SynthContext context, Graphics g, int x, int y, int w, int h) {
        Graphics2D g2 = (Graphics2D)g;
        g2.fillRect(x, y, w, h);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(context.getStyle().getColor(context, ColorType.FOREGROUND));
        g2.setStroke(THIN_STROKE);
        g2.drawRect(x, y, w, h);
    }

	/** Make an existing color transparent. */
    private static Color createTransparentColor(Color color) {
        return new Color(color.getRed(), color.getGreen(), color.getBlue(), 0x88);
    }

	@Override
	public void paintPanelBackground(SynthContext context, Graphics graphics, int x, int y, int w, int h) {
		paintEmptyBorder(context, graphics, x, y, w, h);
	}

	@Override
	public void paintToolBarBackground(SynthContext synthContext, Graphics graphics, int i, int i1, int i2, int i3) {
		paintPanelBackground(synthContext, graphics, i, i1, i2, i3);
	}

	@Override
    public void paintTextFieldBackground(SynthContext context,Graphics g, int x, int y,int w, int h) {
        paintBorder(context, g, x, y, w, h);
    }
    
    @Override
    public void paintTextAreaBackground(SynthContext context, Graphics graphics, int x, int y, int w, int h) {
        paintBorder(context, graphics,x ,y ,w ,h);
    }

    @Override
    public void paintPasswordFieldBackground(SynthContext context, Graphics graphics, int x, int y, int w, int h) {
        paintBorder(context, graphics,x ,y ,w ,h);
    }

    @Override
    public void paintListBackground(SynthContext context, Graphics graphics, int x, int y, int w, int h) {
        paintBorder(context, graphics, x, y, w, h);
    }

    @Override
    public void paintEditorPaneBackground(SynthContext context, Graphics graphics, int x, int y, int w, int h) {
        paintBorder(context, graphics,x ,y ,w ,h);
    }

    @Override
    public void paintButtonBackground(SynthContext context,Graphics graphics, int x, int y,int w, int h) {
    	paintEmptyBorder(context, graphics,x ,y ,w ,h);
    }

    @Override
    public void paintScrollBarThumbBackground(SynthContext context, Graphics g, int x, int y, int w, int h, int i4) {
        Graphics2D g2 = (Graphics2D)g;
        g2.setColor(createTransparentColor(context.getStyle().getColor(context, ColorType.BACKGROUND).darker().darker()));
        g2.fillRect(x, y, w, h);
    }

    @Override
    public void paintComboBoxBackground(SynthContext context, Graphics graphics, int x, int y, int w, int h) {
        paintVerticalGradient(context,graphics,x,y,w,h);
    }

	@Override
    public void paintProgressBarBackground(SynthContext context, Graphics graphics, int x, int y, int w, int h) {
		paintEmptyBorder(context, graphics, x, y, w, h);
        paintVerticalGradient(context, graphics, x, y, (int)(w * ((JProgressBar) context.getComponent()).getPercentComplete()), h);
    }

    @Override
    public void paintSpinnerBackground(SynthContext context, Graphics graphics, int x, int y, int w, int h) {
        paintBorder(context, graphics,x ,y ,w ,h);
    }
}