package apjava.server.view;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import apjava.lib.gui.Form;


public class MainForm {
	private JPanel pannel;
	private JPanel pannel2;
	private JButton submit;
	private JTextArea textArea;
	private JTextField inputText;
	private JScrollPane scrollPane;
	// kevin implementation
	private JButton startserver;
	private JButton stopserver;
	private JButton clearserver;
	private JButton restartserver;
	private JPanel panel3;
	

	MainForm() {
		setPannel(new JPanel());
		setPannel2(new JPanel(new GridLayout(0,1)));
		setSubmit(new JButton("Send"));
		setInputText(new JTextField(40));
		this.inputText.setText("Command line user only...");
		setTextArea(new JTextArea());
		
		//kevin implementation
		setPanel3(new JPanel());
		setStartserver(new JButton("  Start   "));
		setStopserver(new JButton("   Stop  "));
		setClearserver(new JButton("  Clear  "));
		setRestartserver(new JButton("Restart"));
		addToPanel(pannel,panel3);
	}

	private void addToPanel(JPanel pannel ,JPanel panel3) {
		
		this.pannel.add(getInputText());
		this.pannel.add(getSubmit());
		this.pannel.setLayout(new BoxLayout(this.pannel, BoxLayout.LINE_AXIS));
		
		this.panel3.add(getStartserver());
		this.panel3.add(Form.createBox(10, 10));
		this.panel3.add(getRestartserver());
		this.panel3.add(Form.createBox(10, 10));
		this.panel3.add(getClearserver());
		this.panel3.add(Form.createBox(10, 10));
		this.panel3.add(getStopserver());
		this.panel3.setLayout(new BoxLayout(this.panel3, BoxLayout.Y_AXIS));
		this.panel3.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
		
		this.textArea.setEditable(false);
		this.textArea.setLineWrap(true);
		this.pannel.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
		this.textArea.setBackground(Color.BLACK);
		//set foreground color for white text
		this.textArea.setForeground(Color.WHITE);
		this.setScrollPane(new JScrollPane(getTextArea()));
		this.pannel2.add(getScrollPane());
		this.pannel2.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
		
		
	}

	public JPanel getPannel(){
		return pannel;
	}

	public void setPannel(JPanel pannel) {
		this.pannel = pannel;
	}

	public JButton getSubmit() {
		return submit;
	}

	public void setSubmit(JButton submit) {
		this.submit = submit;
	}

	public JTextField getInputText() {
		return inputText;
	}

	public void setInputText(JTextField inputText) {
		this.inputText = inputText;
	}

	public JTextArea getTextArea() {
		return textArea;
	}

	public void setTextArea(JTextArea textArea) {
		this.textArea = textArea;
	}

	public JPanel getPannel2() {
		return pannel2;
	}

	public void setPannel2(JPanel pannel2) {
		this.pannel2 = pannel2;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JButton getStartserver() {
		return startserver;
	}

	public void setStartserver(JButton startserver) {
		this.startserver = startserver;
	}

	public JButton getStopserver() {
		return stopserver;
	}

	public void setStopserver(JButton stopserver) {
		this.stopserver = stopserver;
	}

	public JButton getClearserver() {
		return clearserver;
	}

	public void setClearserver(JButton clearserver) {
		this.clearserver = clearserver;
	}

	public JButton getRestartserver() {
		return restartserver;
	}

	public void setRestartserver(JButton restartserver) {
		this.restartserver = restartserver;
	}

	public JPanel getPanel3() {
		return panel3;
	}

	public void setPanel3(JPanel panel3) {
		this.panel3 = panel3;
	}

}
