package apjava.lib;

import java.io.*;

public class DirFilterWatcher implements FileFilter {
  private String [] filters;

  public DirFilterWatcher() {
    this.filters = null;
  }

  public DirFilterWatcher(String [] filter) {
    this.filters = filter;
  }

  public boolean accept(File file) {
    if (filters == null) {
      return true;
    }
    for(String filter : filters) {
	    if(file.getName().endsWith(filter)){
	    	 return true;
	    }
    }
    return false;
  }
}